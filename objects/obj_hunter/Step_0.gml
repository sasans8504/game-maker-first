/// @DnDAction : YoYo Games.Instances.Inherit_Event
/// @DnDVersion : 1
/// @DnDHash : 3651FC08
event_inherited();

/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 63B12F96
/// @DnDArgument : "obj" "obj_ship"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "77f7923c-2459-4571-bc3f-515ee8648a72"
var l63B12F96_0 = false;
l63B12F96_0 = instance_exists(obj_ship);
if(!l63B12F96_0)
{
	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 41F4259F
	/// @DnDParent : 63B12F96
	exit;
}

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 5CDD64F9
/// @DnDInput : 4
/// @DnDArgument : "var" "dist"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "function" "point_distance"
/// @DnDArgument : "arg" "x"
/// @DnDArgument : "arg_1" "y"
/// @DnDArgument : "arg_2" "obj_ship.x"
/// @DnDArgument : "arg_3" "obj_ship.y"
var dist = point_distance(x, y, obj_ship.x, obj_ship.y);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4FB126FB
/// @DnDArgument : "var" "dist"
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "200"
if(dist <= 200)
{
	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 097771DB
	/// @DnDInput : 4
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "var" "dir"
	/// @DnDArgument : "var_temp" "1"
	/// @DnDArgument : "function" "point_direction"
	/// @DnDArgument : "arg" "x"
	/// @DnDArgument : "arg_1" "y"
	/// @DnDArgument : "arg_2" "obj_ship.x"
	/// @DnDArgument : "arg_3" "obj_ship.y"
	var dir = point_direction(x, y, obj_ship.x, obj_ship.y);

	/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
	/// @DnDVersion : 1
	/// @DnDHash : 63518678
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "angle" "dir"
	image_angle = dir;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1D09028A
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "expr" "dir"
	/// @DnDArgument : "var" "direction"
	direction = dir;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 577AA5E8
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "var" "dist"
	/// @DnDArgument : "op" "1"
	/// @DnDArgument : "value" "150"
	if(dist < 150)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6F29AEF3
		/// @DnDParent : 577AA5E8
		/// @DnDArgument : "expr" "-0.05"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "speed"
		speed += -0.05;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 4F9622D2
	/// @DnDParent : 4FB126FB
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 5EEFBE23
		/// @DnDParent : 4F9622D2
		/// @DnDArgument : "expr" "0.01"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "speed"
		speed += 0.01;
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7DA8578F
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "bulletCounter"
	bulletCounter += 1;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 44210A2A
	/// @DnDParent : 4FB126FB
	/// @DnDArgument : "var" "bulletCounter"
	/// @DnDArgument : "op" "4"
	/// @DnDArgument : "value" "40"
	if(bulletCounter >= 40)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3FF192EE
		/// @DnDParent : 44210A2A
		/// @DnDArgument : "var" "bulletCounter"
		bulletCounter = 0;
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 12CD9AC9
		/// @DnDInput : 2
		/// @DnDParent : 44210A2A
		/// @DnDArgument : "script" "create_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "8"
		/// @DnDSaveInfo : "script" "a9759563-8df4-4eba-a189-1d8d1b4f236a"
		script_execute(create_bullet, dir, 8);
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 6DB37C9C
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4C91233D
	/// @DnDParent : 6DB37C9C
	/// @DnDArgument : "expr" "2"
	/// @DnDArgument : "var" "speed"
	speed = 2;
}