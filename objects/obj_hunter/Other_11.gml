/// @DnDAction : YoYo Games.Instances.Inherit_Event
/// @DnDVersion : 1
/// @DnDHash : 554A73BA
event_inherited();

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 706778C2
/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "hunter_num"
with(obj_game) {
hunter_num += -1;

}