{
    "id": "7e1f5ccd-af91-4b04-82e4-1d28d58a6d61",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hunter",
    "eventList": [
        {
            "id": "0d50b90d-6a98-410b-9f14-46730ff82fe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e1f5ccd-af91-4b04-82e4-1d28d58a6d61"
        },
        {
            "id": "d05ce395-0b95-485d-99e9-ef7fdb2a2e27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e1f5ccd-af91-4b04-82e4-1d28d58a6d61"
        },
        {
            "id": "2b7024c2-cec7-4677-90b3-80ad2ad2eaeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7e1f5ccd-af91-4b04-82e4-1d28d58a6d61"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e3073e3d-55c4-4327-a93d-a5f731436a53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "468b9ff8-a59d-4abb-93b3-65e9d9dc4ddf",
    "visible": true
}