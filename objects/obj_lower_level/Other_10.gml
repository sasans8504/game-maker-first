/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 16F88CAA
/// @DnDApplyTo : 5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6
/// @DnDArgument : "var" "level"
/// @DnDArgument : "op" "2"
with(obj_level) var l16F88CAA_0 = level > 0;
if(l16F88CAA_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4DAA9462
	/// @DnDApplyTo : 5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6
	/// @DnDParent : 16F88CAA
	/// @DnDArgument : "expr" "-1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "level"
	with(obj_level) {
	level += -1;
	
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 4A2B8CCD
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2A660CEE
	/// @DnDApplyTo : 5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6
	/// @DnDParent : 4A2B8CCD
	/// @DnDArgument : "expr" "highest_level"
	/// @DnDArgument : "var" "level"
	with(obj_level) {
	level = highest_level;
	
	}
}