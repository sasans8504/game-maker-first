{
    "id": "8d5fa3e1-023e-497b-bc4f-0fbcdc4601dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lower_level",
    "eventList": [
        {
            "id": "3d3148fc-b3f9-4dda-9d18-403770957746",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d5fa3e1-023e-497b-bc4f-0fbcdc4601dc"
        },
        {
            "id": "7f8e0977-8858-4617-9eab-ddcfef495666",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8d5fa3e1-023e-497b-bc4f-0fbcdc4601dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
    "visible": true
}