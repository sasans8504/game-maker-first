/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 34D22EF8
/// @DnDArgument : "var" "powerup"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "type" "1"
/// @DnDArgument : "max" "5"
var powerup = floor(random_range(0, 5 + 1));

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 22590606
/// @DnDArgument : "expr" "powerup"
/// @DnDArgument : "var" "image_index"
image_index = powerup;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 101BBDE8
/// @DnDArgument : "steps" "5 * 60"
alarm_set(0, 5 * 60);

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 111C7DC8
/// @DnDArgument : "colour" "$FF00FF00"
image_blend = $FF00FF00 & $ffffff;
image_alpha = ($FF00FF00 >> 24) / $ff;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 0B42B94C
/// @DnDArgument : "speed" "0"
image_speed = 0;