{
    "id": "53209f28-9491-4e74-997e-5e2e37e07d53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powerup",
    "eventList": [
        {
            "id": "0daef337-edf8-451a-acf5-e583efcd8b54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53209f28-9491-4e74-997e-5e2e37e07d53"
        },
        {
            "id": "c7a919d1-dbe7-4f5c-a377-3c05f7141781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "53209f28-9491-4e74-997e-5e2e37e07d53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
    "visible": true
}