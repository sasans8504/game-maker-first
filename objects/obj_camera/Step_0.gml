/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 0BB9F92B
/// @DnDArgument : "obj" "target"
var l0BB9F92B_0 = false;
l0BB9F92B_0 = instance_exists(target);
if(l0BB9F92B_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6B66F685
	/// @DnDInput : 2
	/// @DnDParent : 0BB9F92B
	/// @DnDArgument : "expr" "clamp(target.x - (cameraWidth/2), 0, room_width - cameraWidth)"
	/// @DnDArgument : "expr_1" "clamp(target.y - (cameraHeight/2), 0, room_height - cameraHeight)"
	/// @DnDArgument : "var" "cameraX"
	/// @DnDArgument : "var_1" "cameraY"
	cameraX = clamp(target.x - (cameraWidth/2), 0, room_width - cameraWidth);
	cameraY = clamp(target.y - (cameraHeight/2), 0, room_height - cameraHeight);

	/// @DnDAction : YoYo Games.Random.Get_Random_Number
	/// @DnDVersion : 1
	/// @DnDHash : 7FFB45F1
	/// @DnDParent : 0BB9F92B
	/// @DnDArgument : "var" "x_shake"
	/// @DnDArgument : "var_temp" "1"
	/// @DnDArgument : "type" "1"
	/// @DnDArgument : "min" "-cameraShake"
	/// @DnDArgument : "max" "cameraShake"
	var x_shake = floor(random_range(-cameraShake, cameraShake + 1));

	/// @DnDAction : YoYo Games.Random.Get_Random_Number
	/// @DnDVersion : 1
	/// @DnDHash : 6B4B0406
	/// @DnDParent : 0BB9F92B
	/// @DnDArgument : "var" "y_shake"
	/// @DnDArgument : "var_temp" "1"
	/// @DnDArgument : "type" "1"
	/// @DnDArgument : "min" "-cameraShake"
	/// @DnDArgument : "max" "cameraShake"
	var y_shake = floor(random_range(-cameraShake, cameraShake + 1));

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7DC0553E
	/// @DnDParent : 0BB9F92B
	/// @DnDArgument : "var" "cameraShake"
	/// @DnDArgument : "op" "2"
	if(cameraShake > 0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6183D3D3
		/// @DnDParent : 7DC0553E
		/// @DnDArgument : "expr" "-0.2"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "cameraShake"
		cameraShake += -0.2;
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 31C28757
		/// @DnDParent : 7DC0553E
		/// @DnDArgument : "var" "cameraShake"
		/// @DnDArgument : "op" "1"
		if(cameraShake < 0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 5FF17FC0
			/// @DnDParent : 31C28757
			/// @DnDArgument : "var" "cameraShake"
			cameraShake = 0;
		}
	}

	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 1E5A6EF0
	/// @DnDInput : 3
	/// @DnDParent : 0BB9F92B
	/// @DnDArgument : "function" "camera_set_view_pos"
	/// @DnDArgument : "arg" "view_camera[0]"
	/// @DnDArgument : "arg_1" "cameraX + x_shake"
	/// @DnDArgument : "arg_2" "cameraY + y_shake"
	camera_set_view_pos(view_camera[0], cameraX + x_shake, cameraY + y_shake);
}

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 5D21ED18
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_x"
/// @DnDArgument : "arg" ""Parallax_0""
/// @DnDArgument : "arg_1" "cameraX * 0.98"
layer_x("Parallax_0", cameraX * 0.98);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 3C3BBB88
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_y"
/// @DnDArgument : "arg" ""Parallax_0""
/// @DnDArgument : "arg_1" "cameraY * 0.98"
layer_y("Parallax_0", cameraY * 0.98);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 6F59F5DD
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_x"
/// @DnDArgument : "arg" ""Parallax_1""
/// @DnDArgument : "arg_1" "cameraX * 0.9"
layer_x("Parallax_1", cameraX * 0.9);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 49877262
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_y"
/// @DnDArgument : "arg" ""Parallax_1""
/// @DnDArgument : "arg_1" "cameraY * 0.9"
layer_y("Parallax_1", cameraY * 0.9);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 793390CB
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_x"
/// @DnDArgument : "arg" ""Parallax_2""
/// @DnDArgument : "arg_1" "cameraX * 0.84"
layer_x("Parallax_2", cameraX * 0.84);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 04569775
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_y"
/// @DnDArgument : "arg" ""Parallax_2""
/// @DnDArgument : "arg_1" "cameraY * 0.84"
layer_y("Parallax_2", cameraY * 0.84);