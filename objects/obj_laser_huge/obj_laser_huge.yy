{
    "id": "75d7e375-26c7-40d1-a3b4-c0ae707c1833",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laser_huge",
    "eventList": [
        {
            "id": "4e48ad38-73aa-44a7-90f7-0f4b6e72e570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75d7e375-26c7-40d1-a3b4-c0ae707c1833"
        },
        {
            "id": "e3529a82-981d-433c-a60b-32bd323086ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "75d7e375-26c7-40d1-a3b4-c0ae707c1833"
        },
        {
            "id": "38c5489d-591f-4d6e-832e-195ba1d86d6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "75d7e375-26c7-40d1-a3b4-c0ae707c1833"
        },
        {
            "id": "c17d727d-f37e-4131-a785-6a9060aaf685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75d7e375-26c7-40d1-a3b4-c0ae707c1833"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
    "visible": true
}