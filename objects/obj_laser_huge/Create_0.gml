/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 7A08AF9A
image_speed = 1;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 11B5625C
/// @DnDArgument : "var" "speed"
speed = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 159D871C
/// @DnDInput : 2
/// @DnDArgument : "expr" "undefined"
/// @DnDArgument : "expr_1" "undefined"
/// @DnDArgument : "var" "faction"
/// @DnDArgument : "var_1" "creator"
faction = undefined;
creator = undefined;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 37F9A81C
/// @DnDApplyTo : 1ea3458d-104e-4479-ad3d-b06f4b8c94c7
/// @DnDArgument : "expr" "4"
/// @DnDArgument : "var" "cameraShake"
with(obj_camera) {
cameraShake = 4;

}

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 2C888AC9
/// @DnDArgument : "steps" "80"
alarm_set(0, 80);