/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 21D3B324
/// @DnDArgument : "var" "position_meeting(mouse_x, mouse_y, other)"
/// @DnDArgument : "value" "true"
if(position_meeting(mouse_x, mouse_y, other) == true)
{
	/// @DnDAction : YoYo Games.Instances.Color_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 023E0B2B
	/// @DnDParent : 21D3B324
	/// @DnDArgument : "colour" "$FF999999"
	image_blend = $FF999999 & $ffffff;
	image_alpha = ($FF999999 >> 24) / $ff;

	/// @DnDAction : YoYo Games.Instances.Call_User_Event
	/// @DnDVersion : 1
	/// @DnDHash : 1D396077
	/// @DnDParent : 21D3B324
	event_user(0);
}