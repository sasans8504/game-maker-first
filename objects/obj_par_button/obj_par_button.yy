{
    "id": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_par_button",
    "eventList": [
        {
            "id": "1c811be2-e8c4-4ff8-b16f-bc7850da95f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867"
        },
        {
            "id": "4ad85651-58de-4189-9ce0-fccd1fd5c955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867"
        },
        {
            "id": "af756fda-7859-43ce-8453-2ec8424e5fd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}