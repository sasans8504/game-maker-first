/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 376F73C3
/// @DnDInput : 6
/// @DnDArgument : "expr" "2"
/// @DnDArgument : "expr_1" "random_range(0, 359)"
/// @DnDArgument : "expr_2" "direction"
/// @DnDArgument : "expr_3" "1"
/// @DnDArgument : "expr_5" "true"
/// @DnDArgument : "var" "speed"
/// @DnDArgument : "var_1" "direction"
/// @DnDArgument : "var_2" "image_angle"
/// @DnDArgument : "var_3" "hp"
/// @DnDArgument : "var_4" "exhaustCounter"
/// @DnDArgument : "var_5" "timeout"
speed = 2;
direction = random_range(0, 359);
image_angle = direction;
hp = 1;
exhaustCounter = 0;
timeout = true;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 23DE48EF
/// @DnDDisabled : 1
/// @DnDArgument : "steps" "60 * 3"