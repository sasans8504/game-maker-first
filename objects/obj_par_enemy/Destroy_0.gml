/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 00FEB523
/// @DnDArgument : "soundid" "snd_die"
/// @DnDSaveInfo : "soundid" "e7c18019-e5a2-450e-9e16-ad326d1cff42"
audio_play_sound(snd_die, 0, 0);

/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 24EF63DD
/// @DnDInput : 2
/// @DnDArgument : "var" "xx"
/// @DnDArgument : "value" "x"
/// @DnDArgument : "var_1" "yy"
/// @DnDArgument : "value_1" "y"
var xx = x;
var yy = y;

/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
/// @DnDVersion : 1
/// @DnDHash : 0100F3B7
/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
/// @DnDArgument : "x" "xx"
/// @DnDArgument : "y" "yy"
/// @DnDArgument : "system" "partSys"
/// @DnDArgument : "type" "partTypeEnemyDebris"
with(obj_particles) part_particles_create(partSys, xx, yy, partTypeEnemyDebris, 10);

/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 2378A8F8
/// @DnDArgument : "var" "chance"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "type" "1"
/// @DnDArgument : "max" "5"
var chance = floor(random_range(0, 5 + 1));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 16AA639C
/// @DnDArgument : "var" "chance"
if(chance == 0)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 2D17FB18
	/// @DnDParent : 16AA639C
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_powerup"
	/// @DnDSaveInfo : "objectid" "53209f28-9491-4e74-997e-5e2e37e07d53"
	instance_create_layer(x + 0, y + 0, "Instances", obj_powerup);
}

/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 30394D9C
/// @DnDArgument : "expr" "object_index"
var l30394D9C_0 = object_index;
switch(l30394D9C_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 30B42F99
	/// @DnDParent : 30394D9C
	/// @DnDArgument : "const" "obj_raider"
	case obj_raider:
		/// @DnDAction : YoYo Games.Instance Variables.Set_Score
		/// @DnDVersion : 1
		/// @DnDHash : 536E1544
		/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
		/// @DnDParent : 30B42F99
		/// @DnDArgument : "score" "15"
		/// @DnDArgument : "score_relative" "1"
		with(obj_game) {
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		__dnd_score += real(15);
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 12243B61
	/// @DnDParent : 30394D9C
	/// @DnDArgument : "const" "obj_hunter"
	case obj_hunter:
		/// @DnDAction : YoYo Games.Instance Variables.Set_Score
		/// @DnDVersion : 1
		/// @DnDHash : 40D2F948
		/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
		/// @DnDParent : 12243B61
		/// @DnDArgument : "score" "30"
		/// @DnDArgument : "score_relative" "1"
		with(obj_game) {
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		__dnd_score += real(30);
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 4C880DDF
	/// @DnDParent : 30394D9C
	/// @DnDArgument : "const" "obj_brute"
	case obj_brute:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2E44570D
		/// @DnDApplyTo : 1ea3458d-104e-4479-ad3d-b06f4b8c94c7
		/// @DnDParent : 4C880DDF
		/// @DnDArgument : "expr" "4"
		/// @DnDArgument : "var" "cameraShake"
		with(obj_camera) {
		cameraShake = 4;
		
		}
	
		/// @DnDAction : YoYo Games.Instance Variables.Set_Score
		/// @DnDVersion : 1
		/// @DnDHash : 75DB1BD8
		/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
		/// @DnDParent : 4C880DDF
		/// @DnDArgument : "score" "50"
		/// @DnDArgument : "score_relative" "1"
		with(obj_game) {
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		__dnd_score += real(50);
		}
		break;
}

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 6C7A24E4
/// @DnDArgument : "times" "10"
repeat(10)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7CD460B9
	/// @DnDParent : 6C7A24E4
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_debris"
	/// @DnDSaveInfo : "objectid" "60b5cc0e-b7b5-495b-9a3e-41a6c87f10b6"
	instance_create_layer(x + 0, y + 0, "Instances", obj_debris);
}