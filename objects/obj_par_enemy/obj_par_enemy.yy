{
    "id": "e3073e3d-55c4-4327-a93d-a5f731436a53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_par_enemy",
    "eventList": [
        {
            "id": "eac6a1c6-a9ce-4d77-bc44-b30743e9e7ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        },
        {
            "id": "08af0cc9-23ac-4b61-8757-dfc50cfc307e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        },
        {
            "id": "f612db07-4dc5-4678-b5de-2893105c6535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        },
        {
            "id": "f22c9cfb-049d-4b1a-b5e9-a221404549c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        },
        {
            "id": "23a8fb71-8cf4-426e-8044-c68ad1a21047",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        },
        {
            "id": "717cd024-4c85-4798-8e7d-780ba3b08de1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e3073e3d-55c4-4327-a93d-a5f731436a53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "38b2598c-c0f8-4cc6-bf87-641ef2e20e53",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "8e8e88b0-35d0-4ccd-a455-dcd023769085",
            "value": "$FF0000FF"
        }
    ],
    "parentObjectId": "e7da4ad1-efb6-4395-80fd-853b5f4c53d2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}