/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3333771B
/// @DnDArgument : "var" "timeout"
/// @DnDArgument : "value" "true"
if(timeout == true)
{
	/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
	/// @DnDVersion : 1
	/// @DnDHash : 22C5A445
	/// @DnDParent : 3333771B
	/// @DnDArgument : "obj" "obj_ship"
	/// @DnDSaveInfo : "obj" "77f7923c-2459-4571-bc3f-515ee8648a72"
	var l22C5A445_0 = false;
	l22C5A445_0 = instance_exists(obj_ship);
	if(l22C5A445_0)
	{
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 674D1EDE
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "dir"
		/// @DnDArgument : "value" "point_direction(obj_ship.x, obj_ship.y, x, y)"
		var dir = point_direction(obj_ship.x, obj_ship.y, x, y);
	
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0F8EDD4E
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "dis"
		/// @DnDArgument : "value" "point_distance(obj_ship.x, obj_ship.y, x, y)"
		var dis = point_distance(obj_ship.x, obj_ship.y, x, y);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 38B950B1
		/// @DnDInput : 2
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "expr" "lengthdir_x(dis, dir)"
		/// @DnDArgument : "expr_1" "lengthdir_y(dis, dir)"
		/// @DnDArgument : "var" "xx"
		/// @DnDArgument : "var_1" "yy"
		xx = lengthdir_x(dis, dir);
		yy = lengthdir_y(dis, dir);
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 60203BD5
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "xx"
		/// @DnDArgument : "op" "1"
		if(xx < 0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 3E43AD7C
			/// @DnDParent : 60203BD5
			/// @DnDArgument : "var" "xx"
			xx = 0;
		}
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1BC1404D
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "xx"
		/// @DnDArgument : "op" "2"
		/// @DnDArgument : "value" "view_wview[0]"
		if(xx > view_wview[0])
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 5650ACDD
			/// @DnDParent : 1BC1404D
			/// @DnDArgument : "expr" "view_wview[0]"
			/// @DnDArgument : "var" "xx"
			xx = view_wview[0];
		}
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 28A84923
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "yy"
		/// @DnDArgument : "op" "1"
		if(yy < 0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 512636E5
			/// @DnDParent : 28A84923
			/// @DnDArgument : "var" "yy"
			yy = 0;
		}
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1DA12417
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "var" "yy"
		/// @DnDArgument : "op" "2"
		/// @DnDArgument : "value" "view_hview[0]"
		if(yy > view_hview[0])
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 50DB5253
			/// @DnDParent : 1DA12417
			/// @DnDArgument : "expr" "view_hview[0]"
			/// @DnDArgument : "var" "yy"
			yy = view_hview[0];
		}
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 12993956
		/// @DnDParent : 22C5A445
		/// @DnDArgument : "x" "xx"
		/// @DnDArgument : "y" "yy"
		/// @DnDArgument : "rot" "dir"
		/// @DnDArgument : "sprite" "spr_enemy_aim"
		/// @DnDArgument : "col" "image_blend"
		/// @DnDSaveInfo : "sprite" "87d44bf7-8ee9-4e24-8983-d1de7b42e79b"
		draw_sprite_ext(spr_enemy_aim, 0, xx, yy, 1, 1, dir, image_blend & $ffffff, (image_blend >> 24) / $ff);
	}
}