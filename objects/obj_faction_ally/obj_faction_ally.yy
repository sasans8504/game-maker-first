{
    "id": "42b13069-597f-41f5-bba2-407a4c58d124",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_faction_ally",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4d4e6aa7-e310-4f60-a966-59650c52317d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "8e8e88b0-35d0-4ccd-a455-dcd023769085",
            "value": "$FFFFFF00"
        },
        {
            "id": "beaf1bcc-1bcf-4097-a3ab-1e2eb5bce103",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "faa90bfc-fe99-4d6b-bb32-bef2495249fc",
            "value": "\"ally\""
        }
    ],
    "parentObjectId": "bd10105f-8896-4994-92be-077c45253e8d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}