{
    "id": "52668c8c-e78b-4147-a381-5968403917da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "bfddb94d-653d-4524-ac26-a952462f1cb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52668c8c-e78b-4147-a381-5968403917da"
        },
        {
            "id": "4bd66a99-c077-4b46-a7ab-ca017797b9ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52668c8c-e78b-4147-a381-5968403917da"
        },
        {
            "id": "2fdb328f-3067-44aa-969e-bce55874bb91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "52668c8c-e78b-4147-a381-5968403917da"
        },
        {
            "id": "27dc06ee-b2c1-4452-81ae-eb3cee30aa15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "52668c8c-e78b-4147-a381-5968403917da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0ca92070-1db0-4e92-9f4e-09e79d5c0182",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2cf9aae-fa3b-4298-a36c-fca02ddd94d6",
    "visible": true
}