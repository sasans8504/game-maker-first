/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 202763B7
/// @DnDArgument : "var" "chance"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "type" "1"
/// @DnDArgument : "max" "9"
var chance = floor(random_range(0, 9 + 1));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6A47EA45
/// @DnDArgument : "var" "chance"
if(chance == 0)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 5D86CB8E
	/// @DnDParent : 6A47EA45
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_powerup"
	/// @DnDSaveInfo : "objectid" "53209f28-9491-4e74-997e-5e2e37e07d53"
	instance_create_layer(x + 0, y + 0, "Instances", obj_powerup);
}

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 6FBB3C56
/// @DnDArgument : "soundid" "snd_hurt"
/// @DnDSaveInfo : "soundid" "3a05033d-2dfe-4aac-aff8-9608c6bf9071"
audio_play_sound(snd_hurt, 0, 0);

/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 32934CA9
/// @DnDInput : 2
/// @DnDArgument : "var" "xx"
/// @DnDArgument : "value" "x"
/// @DnDArgument : "var_1" "yy"
/// @DnDArgument : "value_1" "y"
var xx = x;
var yy = y;

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 190127C2
/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
with(obj_game) {
	/// @DnDAction : YoYo Games.Instance Variables.Set_Score
	/// @DnDVersion : 1
	/// @DnDHash : 44D98E72
	/// @DnDParent : 190127C2
	/// @DnDArgument : "score" "10"
	/// @DnDArgument : "score_relative" "1"
	if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
	__dnd_score += real(10);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 207CCE4F
/// @DnDArgument : "var" "sprite_index"
/// @DnDArgument : "value" "spr_asteroid_huge"
if(sprite_index == spr_asteroid_huge)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 57D45FD2
	/// @DnDApplyTo : 1ea3458d-104e-4479-ad3d-b06f4b8c94c7
	/// @DnDParent : 207CCE4F
	/// @DnDArgument : "expr" "4"
	/// @DnDArgument : "var" "cameraShake"
	with(obj_camera) {
	cameraShake = 4;
	
	}

	/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
	/// @DnDVersion : 1
	/// @DnDHash : 4D749B70
	/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
	/// @DnDParent : 207CCE4F
	/// @DnDArgument : "x" "xx"
	/// @DnDArgument : "y" "yy"
	/// @DnDArgument : "system" "partSys"
	/// @DnDArgument : "type" "partTypeAsteroidDebris"
	/// @DnDArgument : "number" "12"
	with(obj_particles) part_particles_create(partSys, xx, yy, partTypeAsteroidDebris, 12);

	/// @DnDAction : YoYo Games.Loops.Repeat
	/// @DnDVersion : 1
	/// @DnDHash : 08725435
	/// @DnDParent : 207CCE4F
	/// @DnDArgument : "times" "2"
	repeat(2)
	{
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 73A08C68
		/// @DnDParent : 08725435
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newAsteroid"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_asteroid"
		/// @DnDSaveInfo : "objectid" "52668c8c-e78b-4147-a381-5968403917da"
		var newAsteroid = instance_create_layer(x + 0, y + 0, "Instances", obj_asteroid);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2088AC7E
		/// @DnDParent : 08725435
		/// @DnDArgument : "expr" "spr_asteroid_med"
		/// @DnDArgument : "var" "newAsteroid.sprite_index"
		newAsteroid.sprite_index = spr_asteroid_med;
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4DA09936
/// @DnDArgument : "var" "sprite_index"
/// @DnDArgument : "value" "spr_asteroid_med"
if(sprite_index == spr_asteroid_med)
{
	/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
	/// @DnDVersion : 1
	/// @DnDHash : 775A101A
	/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
	/// @DnDParent : 4DA09936
	/// @DnDArgument : "x" "xx"
	/// @DnDArgument : "y" "yy"
	/// @DnDArgument : "system" "partSys"
	/// @DnDArgument : "type" "partTypeAsteroidDebris"
	/// @DnDArgument : "number" "8"
	with(obj_particles) part_particles_create(partSys, xx, yy, partTypeAsteroidDebris, 8);

	/// @DnDAction : YoYo Games.Loops.Repeat
	/// @DnDVersion : 1
	/// @DnDHash : 09032A9E
	/// @DnDParent : 4DA09936
	/// @DnDArgument : "times" "2"
	repeat(2)
	{
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 4BEBF1EA
		/// @DnDParent : 09032A9E
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newAsteroid"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_asteroid"
		/// @DnDSaveInfo : "objectid" "52668c8c-e78b-4147-a381-5968403917da"
		var newAsteroid = instance_create_layer(x + 0, y + 0, "Instances", obj_asteroid);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0B77ED77
		/// @DnDParent : 09032A9E
		/// @DnDArgument : "expr" "spr_asteroid_small"
		/// @DnDArgument : "var" "newAsteroid.sprite_index"
		newAsteroid.sprite_index = spr_asteroid_small;
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 767AFB6C
else
{
	/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
	/// @DnDVersion : 1
	/// @DnDHash : 423D0575
	/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
	/// @DnDParent : 767AFB6C
	/// @DnDArgument : "x" "xx"
	/// @DnDArgument : "y" "yy"
	/// @DnDArgument : "system" "partSys"
	/// @DnDArgument : "type" "partTypeAsteroidDebris"
	/// @DnDArgument : "number" "4"
	with(obj_particles) part_particles_create(partSys, xx, yy, partTypeAsteroidDebris, 4);
}

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 3221AE39
/// @DnDArgument : "times" "10"
repeat(10)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 29151725
	/// @DnDParent : 3221AE39
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_debris"
	/// @DnDSaveInfo : "objectid" "60b5cc0e-b7b5-495b-9a3e-41a6c87f10b6"
	instance_create_layer(x + 0, y + 0, "Instances", obj_debris);
}