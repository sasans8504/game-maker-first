{
    "id": "77f7923c-2459-4571-bc3f-515ee8648a72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "98cccb6e-83af-44ec-9b55-a649f6d4d8c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "34508870-181d-495d-9f9e-1bcb30743c20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "b982be4c-a365-4c7b-ba6f-fa9703941e32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "08a89294-2fd6-4bba-bf46-1c6d5e81eb04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "d1c44dae-970b-4058-985a-560ca124490e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "53209f28-9491-4e74-997e-5e2e37e07d53",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "81a51a89-9d23-45bc-a126-f15e0bf7d40d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "1e76a8e8-dcbf-4211-b44c-557e05db5f63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "e534a4b5-7038-4376-9f38-82e586e97bed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "516ee976-d5f4-4993-b45d-30fff40c3a19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "bfa1b5f0-8ef4-46ed-83a0-6eff6ca985e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "9ae114ae-5cab-42e1-b05e-5152cbf5e1c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "0ab51734-3610-4098-bfd1-06ff7f3936ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 9,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "27ccada8-655b-4849-90ba-e1d524dbb9d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 9,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "89fde314-3ede-4497-b421-54d452557c90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 114,
            "eventtype": 9,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "885129bb-ca58-4259-a83e-a83c89c71d9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        },
        {
            "id": "c2f13653-1aad-49a5-a6f0-3e696a5e68eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "77f7923c-2459-4571-bc3f-515ee8648a72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42b13069-597f-41f5-bba2-407a4c58d124",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a53beb03-1604-45e4-9b45-25fdd121d3fd",
    "visible": true
}