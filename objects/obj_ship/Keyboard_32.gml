/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1585BB44
/// @DnDArgument : "var" "abs(hspeed)"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "0.1"
if(abs(hspeed) > 0.1)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4BA77B98
	/// @DnDParent : 1585BB44
	/// @DnDArgument : "var" "hspeed"
	/// @DnDArgument : "op" "2"
	if(hspeed > 0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 55747C90
		/// @DnDParent : 4BA77B98
		/// @DnDArgument : "expr" "-0.06"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "hspeed"
		hspeed += -0.06;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 33541097
	/// @DnDParent : 1585BB44
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 5C6E7D84
		/// @DnDParent : 33541097
		/// @DnDArgument : "expr" "0.06"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "hspeed"
		hspeed += 0.06;
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 078B7DF9
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 36F9C052
	/// @DnDParent : 078B7DF9
	/// @DnDArgument : "var" "hspeed"
	hspeed = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 619004AF
/// @DnDArgument : "var" "abs(vspeed)"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "0.1"
if(abs(vspeed) > 0.1)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6F9B3F2F
	/// @DnDParent : 619004AF
	/// @DnDArgument : "var" "vspeed"
	/// @DnDArgument : "op" "2"
	if(vspeed > 0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 145DEB8C
		/// @DnDParent : 6F9B3F2F
		/// @DnDArgument : "expr" "-0.06"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "vspeed"
		vspeed += -0.06;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 5E40B6F1
	/// @DnDParent : 619004AF
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2A78CE7F
		/// @DnDParent : 5E40B6F1
		/// @DnDArgument : "expr" "0.06"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "vspeed"
		vspeed += 0.06;
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 78045572
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 06C6F322
	/// @DnDParent : 78045572
	/// @DnDArgument : "var" "vspeed"
	vspeed = 0;
}