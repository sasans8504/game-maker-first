/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 79B1D706
/// @DnDArgument : "function" "show_debug_message"
/// @DnDArgument : "arg" "hspeed"
show_debug_message(hspeed);

/// @DnDAction : YoYo Games.Movement.Wrap_Room
/// @DnDVersion : 1
/// @DnDHash : 7833708B
/// @DnDArgument : "margin" "sprite_width / 2"
move_wrap(1, 1, sprite_width / 2);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 507AA912
/// @DnDArgument : "expr" "point_direction(obj_ship.x, obj_ship.y, mouse_x, mouse_y)"
/// @DnDArgument : "var" "image_angle"
image_angle = point_direction(obj_ship.x, obj_ship.y, mouse_x, mouse_y);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3763F318
/// @DnDArgument : "expr" "1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "shootReload"
shootReload += 1;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 4DF82758
/// @DnDArgument : "expr" "1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "shiftDelay"
shiftDelay += 1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3ABCF077
/// @DnDArgument : "var" "guns"
/// @DnDArgument : "value" "6"
if(guns == 6)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3D77D128
	/// @DnDParent : 3ABCF077
	/// @DnDArgument : "expr" "80"
	/// @DnDArgument : "var" "shootDelay"
	shootDelay = 80;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 613BD7EE
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 416DAF1C
	/// @DnDParent : 613BD7EE
	/// @DnDArgument : "expr" "10"
	/// @DnDArgument : "var" "shootDelay"
	shootDelay = 10;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4E6E6452
/// @DnDArgument : "var" "boost"
/// @DnDArgument : "value" "true"
if(boost == true)
{
	/// @DnDAction : YoYo Games.Instance Variables.Set_Health
	/// @DnDVersion : 1
	/// @DnDHash : 5E78B523
	/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
	/// @DnDParent : 4E6E6452
	/// @DnDArgument : "health" "-3"
	/// @DnDArgument : "health_relative" "1"
	with(obj_game) {
	if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
	__dnd_health += real(-3);
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 353E24CF
else
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2A573656
	/// @DnDParent : 353E24CF
	/// @DnDArgument : "var" "shiftDelay"
	/// @DnDArgument : "op" "1"
	/// @DnDArgument : "value" "120"
	if(shiftDelay < 120)
	{
		/// @DnDAction : YoYo Games.Instance Variables.If_Health
		/// @DnDVersion : 1
		/// @DnDHash : 025A452B
		/// @DnDParent : 2A573656
		/// @DnDArgument : "op" "1"
		/// @DnDArgument : "value" "100"
		if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
		if(__dnd_health < 100)
		{
			/// @DnDAction : YoYo Games.Instance Variables.Set_Health
			/// @DnDVersion : 1
			/// @DnDHash : 62C708F2
			/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
			/// @DnDParent : 025A452B
			/// @DnDArgument : "health" "1"
			/// @DnDArgument : "health_relative" "1"
			with(obj_game) {
			if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
			__dnd_health += real(1);
			}
		}
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1E61E242
/// @DnDArgument : "var" "shootReload"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "shootDelay"
if(shootReload >= shootDelay)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2D6CA186
	/// @DnDParent : 1E61E242
	/// @DnDArgument : "var" "shootReload"
	shootReload = 0;

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 3EA561E1
	/// @DnDInput : 3
	/// @DnDParent : 1E61E242
	/// @DnDArgument : "script" "create_bullet"
	/// @DnDArgument : "arg" "image_angle"
	/// @DnDArgument : "arg_1" "6"
	/// @DnDArgument : "arg_2" "guns"
	/// @DnDSaveInfo : "script" "a9759563-8df4-4eba-a189-1d8d1b4f236a"
	script_execute(create_bullet, image_angle, 6, guns);
}