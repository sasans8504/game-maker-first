/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 781D978C
/// @DnDArgument : "soundid" "snd_die"
/// @DnDSaveInfo : "soundid" "e7c18019-e5a2-450e-9e16-ad326d1cff42"
audio_play_sound(snd_die, 0, 0);

/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 2B393DA9
/// @DnDInput : 2
/// @DnDArgument : "var" "xx"
/// @DnDArgument : "value" "x"
/// @DnDArgument : "var_1" "yy"
/// @DnDArgument : "value_1" "y"
var xx = x;
var yy = y;

/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
/// @DnDVersion : 1
/// @DnDHash : 11940862
/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
/// @DnDArgument : "x" "xx"
/// @DnDArgument : "y" "yy"
/// @DnDArgument : "system" "partSys"
/// @DnDArgument : "type" "partTypeShipDebris"
with(obj_particles) part_particles_create(partSys, xx, yy, partTypeShipDebris, 10);

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 5380C95E
/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
with(obj_game) {
	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 074878E2
	/// @DnDParent : 5380C95E
	/// @DnDArgument : "steps" "60"
	/// @DnDArgument : "alarm" "1"
	alarm_set(1, 60);
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 5F13A0EB
/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
with(obj_game) {
	/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 19F3FB38
	/// @DnDParent : 5F13A0EB
	/// @DnDArgument : "lives" "-1"
	/// @DnDArgument : "lives_relative" "1"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	__dnd_lives += real(-1);
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 5B788F30
instance_destroy();

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 5645C4BA
/// @DnDArgument : "times" "10"
repeat(10)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 74EE6D2A
	/// @DnDParent : 5645C4BA
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_debris"
	/// @DnDSaveInfo : "objectid" "60b5cc0e-b7b5-495b-9a3e-41a6c87f10b6"
	instance_create_layer(x + 0, y + 0, "Instances", obj_debris);
}