/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 4AFFB287
/// @DnDArgument : "xpos" "obj_ship.x"
/// @DnDArgument : "ypos" "obj_ship.y"
/// @DnDArgument : "objectid" "obj_powerup"
/// @DnDSaveInfo : "objectid" "53209f28-9491-4e74-997e-5e2e37e07d53"
instance_create_layer(obj_ship.x, obj_ship.y, "Instances", obj_powerup);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 4B188371
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "invincible"
invincible = false;