/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 13771CB9
/// @DnDArgument : "var" "shiftDelay"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "120"
if(shiftDelay >= 120)
{
	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 370CF8A0
	/// @DnDParent : 13771CB9
	/// @DnDArgument : "soundid" "snd_boost"
	/// @DnDSaveInfo : "soundid" "eb48b159-0567-454f-92c2-e49053a9cfef"
	audio_play_sound(snd_boost, 0, 0);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 38CFAF1C
	/// @DnDParent : 13771CB9
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "boost"
	boost = true;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 156F44A0
	/// @DnDParent : 13771CB9
	/// @DnDArgument : "var" "shiftDelay"
	shiftDelay = 0;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4FA2868B
	/// @DnDParent : 13771CB9
	/// @DnDArgument : "expr" "3"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "moveSpeed"
	moveSpeed += 3;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 0CE4A655
	/// @DnDParent : 13771CB9
	/// @DnDArgument : "alarm" "2"
	alarm_set(2, 30);
}