/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 1B4D5CF4
draw_self();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 08D45F96
/// @DnDArgument : "var" "invincible"
/// @DnDArgument : "value" "true"
if(invincible == true)
{
	/// @DnDAction : YoYo Games.Common.If_Expression
	/// @DnDVersion : 1
	/// @DnDHash : 57E50F35
	/// @DnDParent : 08D45F96
	/// @DnDArgument : "expr" "alarm[0] > 120 or floor(alarm[0] / 10) mod 2 == 0"
	if(alarm[0] > 120 or floor(alarm[0] / 10) mod 2 == 0)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 03075ABB
		/// @DnDParent : 57E50F35
		/// @DnDArgument : "x_relative" "1"
		/// @DnDArgument : "y_relative" "1"
		/// @DnDArgument : "rot_relative" "1"
		/// @DnDArgument : "sprite" "spr_ship_powerups_strip7"
		/// @DnDArgument : "frame" "5"
		/// @DnDArgument : "col" "$FF00FFFF"
		/// @DnDSaveInfo : "sprite" "9fecd960-4fbf-4a7c-b15e-5185866821be"
		draw_sprite_ext(spr_ship_powerups_strip7, 5, x + 0, y + 0, 1, 1, image_angle + 0, $FF00FFFF & $ffffff, ($FF00FFFF >> 24) / $ff);
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3832843B
/// @DnDArgument : "var" "guns"
/// @DnDArgument : "not" "1"
/// @DnDArgument : "value" "-1"
if(!(guns == -1))
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3910F3F6
	/// @DnDParent : 3832843B
	/// @DnDArgument : "var" "guns"
	/// @DnDArgument : "value" "6"
	if(guns == 6)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 190934B9
		/// @DnDParent : 3910F3F6
		/// @DnDArgument : "x_relative" "1"
		/// @DnDArgument : "y_relative" "1"
		/// @DnDArgument : "rot_relative" "1"
		/// @DnDArgument : "sprite" "spr_ship_powerups_strip7"
		/// @DnDArgument : "frame" "guns"
		/// @DnDSaveInfo : "sprite" "9fecd960-4fbf-4a7c-b15e-5185866821be"
		draw_sprite_ext(spr_ship_powerups_strip7, guns, x + 0, y + 0, 1, 1, image_angle + 0, $FFFFFFFF & $ffffff, ($FFFFFFFF >> 24) / $ff);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 5BB8E735
	/// @DnDParent : 3832843B
	else
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 53EEFB19
		/// @DnDParent : 5BB8E735
		/// @DnDArgument : "x_relative" "1"
		/// @DnDArgument : "y_relative" "1"
		/// @DnDArgument : "rot_relative" "1"
		/// @DnDArgument : "sprite" "spr_ship_powerups_strip7"
		/// @DnDArgument : "frame" "guns"
		/// @DnDArgument : "col" "image_blend"
		/// @DnDSaveInfo : "sprite" "9fecd960-4fbf-4a7c-b15e-5185866821be"
		draw_sprite_ext(spr_ship_powerups_strip7, guns, x + 0, y + 0, 1, 1, image_angle + 0, image_blend & $ffffff, (image_blend >> 24) / $ff);
	}
}