/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 0CBEAB07
/// @DnDInput : 8
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "expr_1" "-1"
/// @DnDArgument : "expr_2" "10"
/// @DnDArgument : "expr_3" "3"
/// @DnDArgument : "expr_4" "120"
/// @DnDArgument : "expr_5" "false"
/// @DnDArgument : "var" "invincible"
/// @DnDArgument : "var_1" "guns"
/// @DnDArgument : "var_2" "shootDelay"
/// @DnDArgument : "var_3" "moveSpeed"
/// @DnDArgument : "var_4" "shiftDelay"
/// @DnDArgument : "var_5" "boost"
/// @DnDArgument : "var_6" "exhaustCounter"
/// @DnDArgument : "var_7" "shootReload"
invincible = false;
guns = -1;
shootDelay = 10;
moveSpeed = 3;
shiftDelay = 120;
boost = false;
exhaustCounter = 0;
shootReload = 0;