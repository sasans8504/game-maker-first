/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55EC09C2
/// @DnDArgument : "var" "other.faction"
/// @DnDArgument : "not" "1"
/// @DnDArgument : "value" "faction"
if(!(other.faction == faction))
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4B7CD2CC
	/// @DnDParent : 55EC09C2
	/// @DnDArgument : "var" "invincible"
	/// @DnDArgument : "value" "true"
	if(invincible == true)
	{
		/// @DnDAction : YoYo Games.Instances.Call_User_Event
		/// @DnDVersion : 1
		/// @DnDHash : 41E2AD4E
		/// @DnDApplyTo : other
		/// @DnDParent : 4B7CD2CC
		/// @DnDArgument : "event" "1"
		with(other) {
		event_user(1);
		}
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 482B5BF8
	/// @DnDParent : 55EC09C2
	else
	{
		/// @DnDAction : YoYo Games.Instances.Call_User_Event
		/// @DnDVersion : 1
		/// @DnDHash : 0EAFD3DD
		/// @DnDParent : 482B5BF8
		/// @DnDArgument : "event" "1"
		event_user(1);
	}
}