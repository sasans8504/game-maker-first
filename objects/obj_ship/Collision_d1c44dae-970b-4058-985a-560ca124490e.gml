/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 7556267B
/// @DnDApplyTo : other
with(other) {
	/// @DnDAction : YoYo Games.Common.Temp_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 33F86E8A
	/// @DnDParent : 7556267B
	/// @DnDArgument : "var" "powerup_type"
	/// @DnDArgument : "value" "image_index"
	var powerup_type = image_index;

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 744CE1DE
	/// @DnDParent : 7556267B
	instance_destroy();
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7DFD3F7D
/// @DnDArgument : "var" "powerup_type"
/// @DnDArgument : "value" "5"
if(powerup_type == 5)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3802DFE1
	/// @DnDParent : 7DFD3F7D
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "invincible"
	invincible = true;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 77F6C10E
	/// @DnDParent : 7DFD3F7D
	/// @DnDArgument : "steps" "8 * 60"
	alarm_set(0, 8 * 60);
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 2BFDCF62
else
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 08F6050B
	/// @DnDParent : 2BFDCF62
	/// @DnDArgument : "var" "powerup_type"
	/// @DnDArgument : "value" "6"
	if(powerup_type == 6)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 21687392
		/// @DnDInput : 2
		/// @DnDParent : 08F6050B
		/// @DnDArgument : "expr" "powerup_type"
		/// @DnDArgument : "expr_1" "100"
		/// @DnDArgument : "var" "guns"
		/// @DnDArgument : "var_1" "shootReload"
		guns = powerup_type;
		shootReload = 100;
	
		/// @DnDAction : YoYo Games.Instances.Set_Alarm
		/// @DnDVersion : 1
		/// @DnDHash : 43FEE38B
		/// @DnDParent : 08F6050B
		/// @DnDArgument : "steps" "3 * 60"
		/// @DnDArgument : "alarm" "1"
		alarm_set(1, 3 * 60);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 6DEF771D
	/// @DnDParent : 2BFDCF62
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 016E2F8A
		/// @DnDParent : 6DEF771D
		/// @DnDArgument : "expr" "powerup_type"
		/// @DnDArgument : "var" "guns"
		guns = powerup_type;
	
		/// @DnDAction : YoYo Games.Instances.Set_Alarm
		/// @DnDVersion : 1
		/// @DnDHash : 5D758E5F
		/// @DnDParent : 6DEF771D
		/// @DnDArgument : "steps" "3 * 60"
		/// @DnDArgument : "alarm" "1"
		alarm_set(1, 3 * 60);
	}
}