/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 4B6BA6A4
/// @DnDArgument : "expr" "room"
var l4B6BA6A4_0 = room;
switch(l4B6BA6A4_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 23B5B969
	/// @DnDParent : 4B6BA6A4
	/// @DnDArgument : "const" "rm_start"
	case rm_start:
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 419A70B8
		/// @DnDParent : 23B5B969
		/// @DnDArgument : "room" "rm_game"
		/// @DnDSaveInfo : "room" "013240e1-a3c9-4f77-81ca-8a6c069eaeb7"
		room_goto(rm_game);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 43159812
	/// @DnDParent : 4B6BA6A4
	/// @DnDArgument : "const" "rm_win"
	case rm_win:
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 43457A70
		/// @DnDParent : 43159812
		/// @DnDArgument : "key" "vk_enter"
		var l43457A70_0;
		l43457A70_0 = keyboard_check_pressed(vk_enter);
		if (l43457A70_0)
		{
			/// @DnDAction : YoYo Games.Game.Restart_Game
			/// @DnDVersion : 1
			/// @DnDHash : 6C59E9B9
			/// @DnDParent : 43457A70
			game_restart();
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 2A36D6F7
	/// @DnDParent : 4B6BA6A4
	/// @DnDArgument : "const" "rm_gameover"
	case rm_gameover:
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 3673E018
		/// @DnDParent : 2A36D6F7
		/// @DnDArgument : "key" "vk_enter"
		var l3673E018_0;
		l3673E018_0 = keyboard_check_pressed(vk_enter);
		if (l3673E018_0)
		{
			/// @DnDAction : YoYo Games.Game.Restart_Game
			/// @DnDVersion : 1
			/// @DnDHash : 79B288EC
			/// @DnDParent : 3673E018
			game_restart();
		}
		break;
}