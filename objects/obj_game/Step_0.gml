/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B428A4B
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_game"
if(room == rm_game)
{
	/// @DnDAction : YoYo Games.Instance Variables.If_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 60F73BC2
	/// @DnDParent : 4B428A4B
	/// @DnDArgument : "op" "3"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	if(__dnd_lives <= 0)
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 1D46D092
		/// @DnDParent : 60F73BC2
		/// @DnDArgument : "soundid" "msc_song"
		/// @DnDSaveInfo : "soundid" "84125c99-fb1b-4f14-8224-e5df8a6d90d4"
		audio_stop_sound(msc_song);
	
		/// @DnDAction : YoYo Games.Instance Variables.If_Score
		/// @DnDVersion : 1
		/// @DnDHash : 3D0CD6DD
		/// @DnDParent : 60F73BC2
		/// @DnDArgument : "op" "4"
		/// @DnDArgument : "value" "1000"
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		if(__dnd_score >= 1000)
		{
			/// @DnDAction : YoYo Games.Audio.Play_Audio
			/// @DnDVersion : 1
			/// @DnDHash : 09F87A1F
			/// @DnDParent : 3D0CD6DD
			/// @DnDArgument : "soundid" "snd_win"
			/// @DnDSaveInfo : "soundid" "f2e57ca7-fce7-4131-98f2-c92a1f3b2031"
			audio_play_sound(snd_win, 0, 0);
		
			/// @DnDAction : YoYo Games.Rooms.Go_To_Room
			/// @DnDVersion : 1
			/// @DnDHash : 739E4665
			/// @DnDParent : 3D0CD6DD
			/// @DnDArgument : "room" "rm_win"
			/// @DnDSaveInfo : "room" "5fa765d1-bf20-47d6-ba95-4b3171838881"
			room_goto(rm_win);
		}
	
		/// @DnDAction : YoYo Games.Common.Else
		/// @DnDVersion : 1
		/// @DnDHash : 66F5E531
		/// @DnDParent : 60F73BC2
		else
		{
			/// @DnDAction : YoYo Games.Rooms.Go_To_Room
			/// @DnDVersion : 1
			/// @DnDHash : 616D2EF8
			/// @DnDParent : 66F5E531
			/// @DnDArgument : "room" "rm_gameover"
			/// @DnDSaveInfo : "room" "3e5d1f10-24e1-4448-a452-8ee79a0bb98e"
			room_goto(rm_gameover);
		
			/// @DnDAction : YoYo Games.Audio.Play_Audio
			/// @DnDVersion : 1
			/// @DnDHash : 5255308B
			/// @DnDParent : 66F5E531
			/// @DnDArgument : "soundid" "snd_lose"
			/// @DnDSaveInfo : "soundid" "575fc862-01a7-4ce0-adfa-809a9597de50"
			audio_play_sound(snd_lose, 0, 0);
		}
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 791E09BD
	/// @DnDParent : 4B428A4B
	/// @DnDArgument : "var" "level"
	/// @DnDArgument : "not" "1"
	if(!(level == 0))
	{
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 42345783
		/// @DnDParent : 791E09BD
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "op" "3"
		if(raider_num <= 0)
		{
			/// @DnDAction : YoYo Games.Common.If_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 73764210
			/// @DnDParent : 42345783
			/// @DnDArgument : "var" "hunter_num"
			if(hunter_num == 0)
			{
				/// @DnDAction : YoYo Games.Common.If_Variable
				/// @DnDVersion : 1
				/// @DnDHash : 6E58FA26
				/// @DnDParent : 73764210
				/// @DnDArgument : "var" "brute_num"
				if(brute_num == 0)
				{
					/// @DnDAction : YoYo Games.Audio.Play_Audio
					/// @DnDVersion : 1
					/// @DnDHash : 69020D78
					/// @DnDParent : 6E58FA26
					/// @DnDArgument : "soundid" "snd_win"
					/// @DnDSaveInfo : "soundid" "f2e57ca7-fce7-4131-98f2-c92a1f3b2031"
					audio_play_sound(snd_win, 0, 0);
				
					/// @DnDAction : YoYo Games.Rooms.Go_To_Room
					/// @DnDVersion : 1
					/// @DnDHash : 0F4EB7AF
					/// @DnDParent : 6E58FA26
					/// @DnDArgument : "room" "rm_win"
					/// @DnDSaveInfo : "room" "5fa765d1-bf20-47d6-ba95-4b3171838881"
					room_goto(rm_win);
				}
			}
		}
	}

	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 171E5F27
	/// @DnDInput : 2
	/// @DnDParent : 4B428A4B
	/// @DnDArgument : "function" "window_mouse_set"
	/// @DnDArgument : "arg" "clamp(window_mouse_get_x(),0,window_get_width())"
	/// @DnDArgument : "arg_1" "clamp(window_mouse_get_y(),0,window_get_height())"
	window_mouse_set(clamp(window_mouse_get_x(),0,window_get_width()), clamp(window_mouse_get_y(),0,window_get_height()));
}