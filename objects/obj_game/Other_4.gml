/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 25845E78
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_game"
if(room == rm_game)
{
	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 2E0556C1
	/// @DnDParent : 25845E78
	/// @DnDArgument : "soundid" "msc_song"
	/// @DnDSaveInfo : "soundid" "84125c99-fb1b-4f14-8224-e5df8a6d90d4"
	var l2E0556C1_0 = msc_song;
	if (audio_is_playing(l2E0556C1_0))
	{
		/// @DnDAction : YoYo Games.Audio.Stop_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 5AF05CF3
		/// @DnDParent : 2E0556C1
		/// @DnDArgument : "soundid" "msc_song"
		/// @DnDSaveInfo : "soundid" "84125c99-fb1b-4f14-8224-e5df8a6d90d4"
		audio_stop_sound(msc_song);
	}

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 6C7F1950
	/// @DnDParent : 25845E78
	/// @DnDArgument : "soundid" "msc_song"
	/// @DnDArgument : "loop" "1"
	/// @DnDSaveInfo : "soundid" "84125c99-fb1b-4f14-8224-e5df8a6d90d4"
	audio_play_sound(msc_song, 0, 1);

	/// @DnDAction : YoYo Games.Instance Variables.Set_Health
	/// @DnDVersion : 1
	/// @DnDHash : 55E4025B
	/// @DnDParent : 25845E78
	/// @DnDArgument : "health" "100"
	
	__dnd_health = real(100);

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 1B48006E
	/// @DnDParent : 25845E78
	/// @DnDArgument : "steps" "60"
	alarm_set(0, 60);

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 6F20B499
	/// @DnDInput : 2
	/// @DnDParent : 25845E78
	/// @DnDArgument : "script" "spawn_off_camera"
	/// @DnDArgument : "arg" "obj_asteroid"
	/// @DnDArgument : "arg_1" "40"
	/// @DnDSaveInfo : "script" "4e270375-1604-405c-8e3d-1811de9d8d5a"
	script_execute(spawn_off_camera, obj_asteroid, 40);

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 2D86544F
	/// @DnDInput : 2
	/// @DnDParent : 25845E78
	/// @DnDArgument : "script" "spawn_off_camera"
	/// @DnDArgument : "arg" "obj_raider"
	/// @DnDArgument : "arg_1" "raider_num"
	/// @DnDSaveInfo : "script" "4e270375-1604-405c-8e3d-1811de9d8d5a"
	script_execute(spawn_off_camera, obj_raider, raider_num);

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 320872E1
	/// @DnDInput : 2
	/// @DnDParent : 25845E78
	/// @DnDArgument : "script" "spawn_off_camera"
	/// @DnDArgument : "arg" "obj_hunter"
	/// @DnDArgument : "arg_1" "hunter_num"
	/// @DnDSaveInfo : "script" "4e270375-1604-405c-8e3d-1811de9d8d5a"
	script_execute(spawn_off_camera, obj_hunter, hunter_num);

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 77AD7953
	/// @DnDInput : 2
	/// @DnDParent : 25845E78
	/// @DnDArgument : "script" "spawn_off_camera"
	/// @DnDArgument : "arg" "obj_brute"
	/// @DnDArgument : "arg_1" "brute_num"
	/// @DnDSaveInfo : "script" "4e270375-1604-405c-8e3d-1811de9d8d5a"
	script_execute(spawn_off_camera, obj_brute, brute_num);
}