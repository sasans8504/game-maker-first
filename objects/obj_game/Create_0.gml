/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 6A2198E0
/// @DnDInput : 6
/// @DnDArgument : "var" "chinese"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "function" "font_add"
/// @DnDArgument : "arg" ""GenJyuuGothic-Normal.ttf""
/// @DnDArgument : "arg_1" "14"
/// @DnDArgument : "arg_2" "false"
/// @DnDArgument : "arg_3" "false"
/// @DnDArgument : "arg_4" "0"
/// @DnDArgument : "arg_5" "65535"
var chinese = font_add("GenJyuuGothic-Normal.ttf", 14, false, false, 0, 65535);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 33D4F081
/// @DnDArgument : "expr" "chinese"
/// @DnDArgument : "var" "game_font"
game_font = chinese;

/// @DnDAction : YoYo Games.Random.Randomize
/// @DnDVersion : 1
/// @DnDHash : 78378426
randomize();

/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 7459E65D

__dnd_score = real(0);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 3A49D26D
/// @DnDArgument : "lives" "3"

__dnd_lives = real(3);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 53600A68
/// @DnDInput : 3
/// @DnDArgument : "var" "raider_num"
/// @DnDArgument : "var_1" "hunter_num"
/// @DnDArgument : "var_2" "brute_num"
raider_num = 0;
hunter_num = 0;
brute_num = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 4D07C110
/// @DnDArgument : "var" "level"
level = 0;

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 0E2CA637
/// @DnDArgument : "font" "fnt_text"
/// @DnDSaveInfo : "font" "a7193389-479d-4eaf-b8fc-ceb5989fb3a5"
draw_set_font(fnt_text);