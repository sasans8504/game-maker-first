/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 1D1BA988
/// @DnDArgument : "expr" "room"
var l1D1BA988_0 = room;
switch(l1D1BA988_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 473D4B66
	/// @DnDParent : 1D1BA988
	/// @DnDArgument : "const" "rm_start"
	case rm_start:
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 5750DD81
		/// @DnDDisabled : 1
		/// @DnDParent : 473D4B66
		/// @DnDArgument : "room" "rm_game"
		/// @DnDSaveInfo : "room" "013240e1-a3c9-4f77-81ca-8a6c069eaeb7"
	
	
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 13E885AB
		/// @DnDParent : 473D4B66
		/// @DnDArgument : "room" "rm_menu"
		/// @DnDSaveInfo : "room" "6839f093-f9cc-43e8-b19b-1f5afd73991d"
		room_goto(rm_menu);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 48B9510F
	/// @DnDParent : 1D1BA988
	/// @DnDArgument : "const" "rm_win"
	case rm_win:
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 29EE40D7
		/// @DnDParent : 48B9510F
		/// @DnDArgument : "key" "vk_enter"
		var l29EE40D7_0;
		l29EE40D7_0 = keyboard_check_pressed(vk_enter);
		if (l29EE40D7_0)
		{
			/// @DnDAction : YoYo Games.Game.Restart_Game
			/// @DnDVersion : 1
			/// @DnDHash : 02A763A5
			/// @DnDParent : 29EE40D7
			game_restart();
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 5BD1A03E
	/// @DnDParent : 1D1BA988
	/// @DnDArgument : "const" "rm_gameover"
	case rm_gameover:
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 293BEF68
		/// @DnDParent : 5BD1A03E
		/// @DnDArgument : "key" "vk_enter"
		var l293BEF68_0;
		l293BEF68_0 = keyboard_check_pressed(vk_enter);
		if (l293BEF68_0)
		{
			/// @DnDAction : YoYo Games.Game.Restart_Game
			/// @DnDVersion : 1
			/// @DnDHash : 595D2773
			/// @DnDParent : 293BEF68
			game_restart();
		}
		break;
}