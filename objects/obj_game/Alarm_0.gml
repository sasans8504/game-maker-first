/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5F379E21
/// @DnDArgument : "var" "room"
/// @DnDArgument : "not" "1"
/// @DnDArgument : "value" "rm_game"
if(!(room == rm_game))
{
	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 3C3232DE
	/// @DnDParent : 5F379E21
	exit;
}

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 19A481CF
/// @DnDArgument : "steps" "room_speed*1"
alarm_set(0, room_speed*1);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 16FF1AA7
/// @DnDInput : 2
/// @DnDArgument : "script" "spawn_off_camera"
/// @DnDArgument : "arg" "obj_asteroid"
/// @DnDArgument : "arg_1" "1"
/// @DnDSaveInfo : "script" "4e270375-1604-405c-8e3d-1811de9d8d5a"
script_execute(spawn_off_camera, obj_asteroid, 1);