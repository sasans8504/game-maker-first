{
    "id": "46fbed47-96ae-4a19-80e5-d989fb4d07b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "2c2708a8-24bf-4e7b-a419-d4891fac0b88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "f27f6ca2-358c-4ba8-8d00-63291db9e746",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "05db085c-c9f5-432d-bc2e-0c051dde4abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "fcd8245a-dccd-4cae-aa88-ee7b3d20f781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "34cc4be9-aaf3-4cd2-b8ba-22296346b5e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "1d4b86ea-461b-46d7-8d33-2d12ec8e1887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "c04b5ec8-725b-4964-8336-b6810d203458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "4cba3bf7-c9af-4dc3-b1c5-cbb7daa41e02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        },
        {
            "id": "7cf26b4f-7afb-46bd-968e-949342d8ca9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "46fbed47-96ae-4a19-80e5-d989fb4d07b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}