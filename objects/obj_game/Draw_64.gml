/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 2F199072
/// @DnDArgument : "expr" "room"
var l2F199072_0 = room;
switch(l2F199072_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 39D3CDFA
	/// @DnDParent : 2F199072
	/// @DnDArgument : "const" "rm_win"
	case rm_win:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 18481C41
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 50944441
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "color" "$FF00FF00"
		draw_set_colour($FF00FF00 & $ffffff);
		var l50944441_0=($FF00FF00 >> 24);
		draw_set_alpha(l50944441_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 35A7347E
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""YOU WIN""
		draw_text_transformed(250, 150, string("YOU WIN") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 1C69528F
		/// @DnDParent : 39D3CDFA
		draw_set_colour($FFFFFFFF & $ffffff);
		var l1C69528F_0=($FFFFFFFF >> 24);
		draw_set_alpha(l1C69528F_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 4D767A6F
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""FINAL SCORE: ""
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(250, 250, string("FINAL SCORE: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 31057AC9
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "320"
		/// @DnDArgument : "caption" ""enemy left:""
		/// @DnDArgument : "var" "raider_num + hunter_num + brute_num"
		draw_text(250, 320, string("enemy left:") + string(raider_num + hunter_num + brute_num));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 31993F2F
		/// @DnDParent : 39D3CDFA
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "400"
		/// @DnDArgument : "caption" "">> PRESS ENTER TO RESTART << ""
		draw_text(250, 400, string(">> PRESS ENTER TO RESTART << ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 513E6FDD
		/// @DnDParent : 39D3CDFA
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 7EC8777B
	/// @DnDParent : 2F199072
	/// @DnDArgument : "const" "rm_start"
	case rm_start:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 39590A57
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 6D48ABE3
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "color" "$FF00FFFF"
		draw_set_colour($FF00FFFF & $ffffff);
		var l6D48ABE3_0=($FF00FFFF >> 24);
		draw_set_alpha(l6D48ABE3_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Font
		/// @DnDVersion : 1
		/// @DnDHash : 43EE83EE
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "font" "game_font"
		draw_set_font(game_font);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 6200D495
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "100"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""宇宙""
		draw_text_transformed(250, 100, string("宇宙") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Font
		/// @DnDVersion : 1
		/// @DnDHash : 2E53C4F9
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "font" "fnt_text"
		/// @DnDSaveInfo : "font" "a7193389-479d-4eaf-b8fc-ceb5989fb3a5"
		draw_set_font(fnt_text);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 040EB092
		/// @DnDParent : 7EC8777B
		draw_set_colour($FFFFFFFF & $ffffff);
		var l040EB092_0=($FFFFFFFF >> 24);
		draw_set_alpha(l040EB092_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 213F9E88
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "caption" ""Defeat all enemy ship to win""
		draw_text(250, 200, string("Defeat all enemy ship to win") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 26E81EFE
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "230"
		/// @DnDArgument : "caption" ""W : move""
		draw_text(250, 230, string("W : move") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 1F6976F0
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""MOUSE : direction""
		draw_text(250, 250, string("MOUSE : direction") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 04C7A2EF
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "270"
		/// @DnDArgument : "caption" ""SHIFT : Boost""
		draw_text(250, 270, string("SHIFT : Boost") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 50C16501
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "290"
		/// @DnDArgument : "caption" ""SPACE : Break""
		draw_text(250, 290, string("SPACE : Break") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 31B9C55F
		/// @DnDParent : 7EC8777B
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "400"
		/// @DnDArgument : "caption" "">> PRESS ENTER TO START << ""
		draw_text(250, 400, string(">> PRESS ENTER TO START << ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 7FF8B402
		/// @DnDParent : 7EC8777B
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 699CC7D6
	/// @DnDParent : 2F199072
	/// @DnDArgument : "const" "rm_game"
	case rm_game:
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 5FE35544
		/// @DnDParent : 699CC7D6
		/// @DnDArgument : "x" "330"
		/// @DnDArgument : "y" "20"
		/// @DnDArgument : "caption" ""enemy left: ""
		/// @DnDArgument : "var" "raider_num + hunter_num + brute_num"
		draw_text(330, 20, string("enemy left: ") + string(raider_num + hunter_num + brute_num));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 3660F893
		/// @DnDParent : 699CC7D6
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "20"
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(20, 20, string("Score: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
		/// @DnDVersion : 1
		/// @DnDHash : 205B97D8
		/// @DnDParent : 699CC7D6
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "40"
		/// @DnDArgument : "sprite" "spr_lives"
		/// @DnDSaveInfo : "sprite" "3beb6ac2-8816-483a-a591-d7322ff8f339"
		var l205B97D8_0 = sprite_get_width(spr_lives);
		var l205B97D8_1 = 0;
		if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
		for(var l205B97D8_2 = __dnd_lives; l205B97D8_2 > 0; --l205B97D8_2) {
			draw_sprite(spr_lives, 0, 20 + l205B97D8_1, 40);
			l205B97D8_1 += l205B97D8_0;
		}
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 440F6E26
		/// @DnDParent : 699CC7D6
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "60"
		/// @DnDArgument : "caption" ""Boost: ""
		draw_text(20, 60, string("Boost: ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Health
		/// @DnDVersion : 1
		/// @DnDHash : 35669E1C
		/// @DnDParent : 699CC7D6
		/// @DnDArgument : "x1" "80"
		/// @DnDArgument : "y1" "65"
		/// @DnDArgument : "x2" "180"
		/// @DnDArgument : "y2" "80"
		/// @DnDArgument : "backcol" "image_blend"
		/// @DnDArgument : "mincol" "$FF0000FF"
		/// @DnDArgument : "maxcol" "$FF00FF00"
		if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
		draw_healthbar(80, 65, 180, 80, __dnd_health, image_blend, $FF0000FF & $FFFFFF, $FF00FF00 & $FFFFFF, 0, ((image_blend>>24) != 0), (($FFFFFFFF>>24) != 0));
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 19C69225
	/// @DnDParent : 2F199072
	/// @DnDArgument : "const" "rm_gameover"
	case rm_gameover:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 27902BB4
		/// @DnDParent : 19C69225
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 5837B66C
		/// @DnDParent : 19C69225
		/// @DnDArgument : "color" "$FF0000FF"
		draw_set_colour($FF0000FF & $ffffff);
		var l5837B66C_0=($FF0000FF >> 24);
		draw_set_alpha(l5837B66C_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 304D7DD7
		/// @DnDParent : 19C69225
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""YOU LOSE""
		draw_text_transformed(250, 150, string("YOU LOSE") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 123E8C9C
		/// @DnDParent : 19C69225
		draw_set_colour($FFFFFFFF & $ffffff);
		var l123E8C9C_0=($FFFFFFFF >> 24);
		draw_set_alpha(l123E8C9C_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 22F0C804
		/// @DnDParent : 19C69225
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""FINAL SCORE: ""
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(250, 250, string("FINAL SCORE: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 781EE3E0
		/// @DnDParent : 19C69225
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "320"
		/// @DnDArgument : "caption" ""enemy left:""
		/// @DnDArgument : "var" "raider_num + hunter_num + brute_num"
		draw_text(250, 320, string("enemy left:") + string(raider_num + hunter_num + brute_num));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 257D127B
		/// @DnDParent : 19C69225
		/// @DnDArgument : "x" "250"
		/// @DnDArgument : "y" "400"
		/// @DnDArgument : "caption" "">> PRESS ENTER TO RESTART << ""
		draw_text(250, 400, string(">> PRESS ENTER TO RESTART << ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 403E0B2A
		/// @DnDParent : 19C69225
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;
}