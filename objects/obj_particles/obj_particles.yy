{
    "id": "749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_particles",
    "eventList": [
        {
            "id": "fc730f60-e934-42ee-a040-95d004db8a81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3"
        },
        {
            "id": "7a994d63-037b-4cf4-a391-aa20c22a2db4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3"
        },
        {
            "id": "c8fa99bb-1a58-42ea-983e-697fa8ea33ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}