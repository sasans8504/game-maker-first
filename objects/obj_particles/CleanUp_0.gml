/// @DnDAction : YoYo Games.Particles.Part_Syst_Destroy
/// @DnDVersion : 1.1
/// @DnDHash : 4F1D4F9D
/// @DnDArgument : "system" "partSys"
part_system_destroy(partSys);

/// @DnDAction : YoYo Games.Particles.Part_Type_Destroy
/// @DnDVersion : 1
/// @DnDHash : 589A032B
/// @DnDArgument : "type" "partTypeExhaust"
part_type_destroy(partTypeExhaust);

/// @DnDAction : YoYo Games.Particles.Part_Type_Destroy
/// @DnDVersion : 1
/// @DnDHash : 298B61CD
/// @DnDArgument : "type" "partTypeEnemyExhaust"
part_type_destroy(partTypeEnemyExhaust);