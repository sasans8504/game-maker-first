/// @DnDAction : YoYo Games.Particles.Part_Syst_Create
/// @DnDVersion : 1.1
/// @DnDHash : 5B20EC04
/// @DnDArgument : "var" "partSys"
partSys = part_system_create_layer("Instances", 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Create
/// @DnDVersion : 1.1
/// @DnDHash : 7E5451F6
/// @DnDComment : player exhaust
/// @DnDArgument : "var" "partTypeExhaust"
partTypeExhaust = part_type_create();
// no blending

/// @DnDAction : YoYo Games.Particles.Part_Type_Sprite
/// @DnDVersion : 1
/// @DnDHash : 54BA1C35
/// @DnDArgument : "type" "partTypeExhaust"
/// @DnDArgument : "sprite" "spr_exhaust"
/// @DnDSaveInfo : "sprite" "f1e69fdd-b447-4daf-9a61-1c3404bb3431"
part_type_sprite(partTypeExhaust, spr_exhaust, true, false, false);

/// @DnDAction : YoYo Games.Particles.Part_Type_Size
/// @DnDVersion : 1
/// @DnDHash : 187324CB
/// @DnDArgument : "type" "partTypeExhaust"
/// @DnDArgument : "minsize" "0.4"
/// @DnDArgument : "maxsize" "0.4"
/// @DnDArgument : "sizeincr" "0.05"
part_type_size(partTypeExhaust, 0.4, 0.4, 0.05, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Color
/// @DnDVersion : 1
/// @DnDHash : 521D5330
/// @DnDArgument : "type" "partTypeExhaust"
/// @DnDArgument : "midcol" "$FFFF00D4"
/// @DnDArgument : "endcol" "$FFE8007F"
part_type_colour3(partTypeExhaust, $FFFFFF & $FFFFFF, $FFFF00D4 & $FFFFFF, $FFE8007F & $FFFFFF);

/// @DnDAction : YoYo Games.Particles.Part_Type_Alpha
/// @DnDVersion : 1
/// @DnDHash : 1240A306
/// @DnDArgument : "type" "partTypeExhaust"
/// @DnDArgument : "end" "0"
part_type_alpha3(partTypeExhaust, 1, 1, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Life
/// @DnDVersion : 1
/// @DnDHash : 1ECA4919
/// @DnDArgument : "typ" "partTypeExhaust"
/// @DnDArgument : "minlife" "20"
/// @DnDArgument : "maxlife" "20"
part_type_life(partTypeExhaust, 20, 20);

/// @DnDAction : YoYo Games.Particles.Part_Type_Create
/// @DnDVersion : 1.1
/// @DnDHash : 1F4D2A48
/// @DnDComment : enemy exhaust
/// @DnDArgument : "var" "partTypeEnemyExhaust"
partTypeEnemyExhaust = part_type_create();
// no blending

/// @DnDAction : YoYo Games.Particles.Part_Type_Sprite
/// @DnDVersion : 1
/// @DnDHash : 62E3A3E7
/// @DnDArgument : "type" "partTypeEnemyExhaust"
/// @DnDArgument : "sprite" "spr_exhaust"
/// @DnDSaveInfo : "sprite" "f1e69fdd-b447-4daf-9a61-1c3404bb3431"
part_type_sprite(partTypeEnemyExhaust, spr_exhaust, true, false, false);

/// @DnDAction : YoYo Games.Particles.Part_Type_Size
/// @DnDVersion : 1
/// @DnDHash : 23BB67DD
/// @DnDArgument : "type" "partTypeEnemyExhaust"
/// @DnDArgument : "minsize" "0.4"
/// @DnDArgument : "maxsize" "0.4"
/// @DnDArgument : "sizeincr" "0.05"
part_type_size(partTypeEnemyExhaust, 0.4, 0.4, 0.05, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Color
/// @DnDVersion : 1
/// @DnDHash : 31EFBF6C
/// @DnDArgument : "type" "partTypeEnemyExhaust"
/// @DnDArgument : "startcol" "$FF00FFFF"
/// @DnDArgument : "midcol" "$FF0292E5"
/// @DnDArgument : "endcol" "$FF0003E2"
part_type_colour3(partTypeEnemyExhaust, $FF00FFFF & $FFFFFF, $FF0292E5 & $FFFFFF, $FF0003E2 & $FFFFFF);

/// @DnDAction : YoYo Games.Particles.Part_Type_Alpha
/// @DnDVersion : 1
/// @DnDHash : 44FD6D5D
/// @DnDArgument : "type" "partTypeEnemyExhaust"
/// @DnDArgument : "end" "0"
part_type_alpha3(partTypeEnemyExhaust, 1, 1, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Life
/// @DnDVersion : 1
/// @DnDHash : 2E8EB042
/// @DnDArgument : "typ" "partTypeEnemyExhaust"
/// @DnDArgument : "minlife" "20"
/// @DnDArgument : "maxlife" "20"
part_type_life(partTypeEnemyExhaust, 20, 20);

/// @DnDAction : YoYo Games.Particles.Part_Type_Create
/// @DnDVersion : 1.1
/// @DnDHash : 15A2B3B9
/// @DnDComment : Asteroid debris
/// @DnDArgument : "var" "partTypeAsteroidDebris"
partTypeAsteroidDebris = part_type_create();
// no blending

/// @DnDAction : YoYo Games.Particles.Part_Type_Sprite
/// @DnDVersion : 1
/// @DnDHash : 46DC20CA
/// @DnDArgument : "type" "partTypeAsteroidDebris"
/// @DnDArgument : "sprite" "spr_asteroid_debris"
/// @DnDSaveInfo : "sprite" "dda92943-1c80-4416-9aa0-babe8345110e"
part_type_sprite(partTypeAsteroidDebris, spr_asteroid_debris, true, false, false);

/// @DnDAction : YoYo Games.Particles.Part_Type_Alpha
/// @DnDVersion : 1
/// @DnDHash : 2E8CBB59
/// @DnDArgument : "type" "partTypeAsteroidDebris"
/// @DnDArgument : "start" ".8"
/// @DnDArgument : "middle" ".8"
/// @DnDArgument : "end" "0"
part_type_alpha3(partTypeAsteroidDebris, .8, .8, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Life
/// @DnDVersion : 1
/// @DnDHash : 3ACA81DF
/// @DnDArgument : "typ" "partTypeAsteroidDebris"
/// @DnDArgument : "minlife" "60"
/// @DnDArgument : "maxlife" "80"
part_type_life(partTypeAsteroidDebris, 60, 80);

/// @DnDAction : YoYo Games.Particles.Part_Type_Direction
/// @DnDVersion : 1
/// @DnDHash : 091EB6D3
/// @DnDArgument : "type" "partTypeAsteroidDebris"
part_type_direction(partTypeAsteroidDebris, 0, 360, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Orientation
/// @DnDVersion : 1
/// @DnDHash : 054B3E21
/// @DnDArgument : "type" "partTypeAsteroidDebris"
part_type_orientation(partTypeAsteroidDebris, 0, 360, 0, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Speed
/// @DnDVersion : 1
/// @DnDHash : 0C5EE406
/// @DnDArgument : "type" "partTypeAsteroidDebris"
/// @DnDArgument : "minspeed" "2"
/// @DnDArgument : "maxspeed" "2.4"
/// @DnDArgument : "incr" "-0.02"
part_type_speed(partTypeAsteroidDebris, 2, 2.4, -0.02, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Create
/// @DnDVersion : 1.1
/// @DnDHash : 0A42656A
/// @DnDComment : ship debris
/// @DnDArgument : "var" "partTypeShipDebris"
partTypeShipDebris = part_type_create();
// no blending

/// @DnDAction : YoYo Games.Particles.Part_Type_Sprite
/// @DnDVersion : 1
/// @DnDHash : 09A451E4
/// @DnDArgument : "type" "partTypeShipDebris"
/// @DnDArgument : "sprite" "spr_ship_debris"
/// @DnDSaveInfo : "sprite" "80bdef99-292c-47e1-993a-6bf558ed988e"
part_type_sprite(partTypeShipDebris, spr_ship_debris, true, false, false);

/// @DnDAction : YoYo Games.Particles.Part_Type_Color
/// @DnDVersion : 1
/// @DnDHash : 5EDE198B
/// @DnDArgument : "type" "partTypeShipDebris"
/// @DnDArgument : "startcol" "$FFFFFF00"
/// @DnDArgument : "midcol" "$FFFFFF00"
/// @DnDArgument : "endcol" "$FFFFFF00"
part_type_colour3(partTypeShipDebris, $FFFFFF00 & $FFFFFF, $FFFFFF00 & $FFFFFF, $FFFFFF00 & $FFFFFF);

/// @DnDAction : YoYo Games.Particles.Part_Type_Size
/// @DnDVersion : 1
/// @DnDHash : 2731DA23
/// @DnDArgument : "type" "partTypeShipDebris"
/// @DnDArgument : "minsize" ".5"
part_type_size(partTypeShipDebris, .5, 1, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Alpha
/// @DnDVersion : 1
/// @DnDHash : 1E6BF79E
/// @DnDArgument : "type" "partTypeShipDebris"
/// @DnDArgument : "start" ".6"
/// @DnDArgument : "middle" ".6"
/// @DnDArgument : "end" "0"
part_type_alpha3(partTypeShipDebris, .6, .6, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Life
/// @DnDVersion : 1
/// @DnDHash : 4BFDFA8B
/// @DnDArgument : "typ" "partTypeShipDebris"
/// @DnDArgument : "minlife" "60"
/// @DnDArgument : "maxlife" "80"
part_type_life(partTypeShipDebris, 60, 80);

/// @DnDAction : YoYo Games.Particles.Part_Type_Direction
/// @DnDVersion : 1
/// @DnDHash : 56C0150F
/// @DnDArgument : "type" "partTypeShipDebris"
part_type_direction(partTypeShipDebris, 0, 360, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Orientation
/// @DnDVersion : 1
/// @DnDHash : 3C189892
/// @DnDArgument : "type" "partTypeShipDebris"
part_type_orientation(partTypeShipDebris, 0, 360, 0, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Speed
/// @DnDVersion : 1
/// @DnDHash : 229B808A
/// @DnDArgument : "type" "partTypeShipDebris"
/// @DnDArgument : "minspeed" "3"
/// @DnDArgument : "maxspeed" "4"
/// @DnDArgument : "incr" "-0.05"
part_type_speed(partTypeShipDebris, 3, 4, -0.05, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Create
/// @DnDVersion : 1.1
/// @DnDHash : 526B17CC
/// @DnDComment : enemy debris
/// @DnDArgument : "var" "partTypeEnemyDebris"
partTypeEnemyDebris = part_type_create();
// no blending

/// @DnDAction : YoYo Games.Particles.Part_Type_Sprite
/// @DnDVersion : 1
/// @DnDHash : 51633B68
/// @DnDArgument : "type" "partTypeEnemyDebris"
/// @DnDArgument : "sprite" "spr_ship_debris"
/// @DnDSaveInfo : "sprite" "80bdef99-292c-47e1-993a-6bf558ed988e"
part_type_sprite(partTypeEnemyDebris, spr_ship_debris, true, false, false);

/// @DnDAction : YoYo Games.Particles.Part_Type_Color
/// @DnDVersion : 1
/// @DnDHash : 1ADBDB22
/// @DnDArgument : "type" "partTypeEnemyDebris"
/// @DnDArgument : "startcol" "$FF0000FF"
/// @DnDArgument : "midcol" "$FF0000FF"
/// @DnDArgument : "endcol" "$FF0000FF"
part_type_colour3(partTypeEnemyDebris, $FF0000FF & $FFFFFF, $FF0000FF & $FFFFFF, $FF0000FF & $FFFFFF);

/// @DnDAction : YoYo Games.Particles.Part_Type_Size
/// @DnDVersion : 1
/// @DnDHash : 0D93D74D
/// @DnDArgument : "type" "partTypeEnemyDebris"
/// @DnDArgument : "minsize" ".5"
part_type_size(partTypeEnemyDebris, .5, 1, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Alpha
/// @DnDVersion : 1
/// @DnDHash : 2FF5FF50
/// @DnDArgument : "type" "partTypeEnemyDebris"
/// @DnDArgument : "start" ".6"
/// @DnDArgument : "middle" ".6"
/// @DnDArgument : "end" "0"
part_type_alpha3(partTypeEnemyDebris, .6, .6, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Life
/// @DnDVersion : 1
/// @DnDHash : 73FE1C24
/// @DnDArgument : "typ" "partTypeEnemyDebris"
/// @DnDArgument : "minlife" "60"
/// @DnDArgument : "maxlife" "80"
part_type_life(partTypeEnemyDebris, 60, 80);

/// @DnDAction : YoYo Games.Particles.Part_Type_Direction
/// @DnDVersion : 1
/// @DnDHash : 4D10A7C2
/// @DnDArgument : "type" "partTypeEnemyDebris"
part_type_direction(partTypeEnemyDebris, 0, 360, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Orientation
/// @DnDVersion : 1
/// @DnDHash : 787C2F44
/// @DnDArgument : "type" "partTypeEnemyDebris"
part_type_orientation(partTypeEnemyDebris, 0, 360, 0, 0, 0);

/// @DnDAction : YoYo Games.Particles.Part_Type_Speed
/// @DnDVersion : 1
/// @DnDHash : 7545AE71
/// @DnDArgument : "type" "partTypeEnemyDebris"
/// @DnDArgument : "minspeed" "3"
/// @DnDArgument : "maxspeed" "4"
/// @DnDArgument : "incr" "-0.05"
part_type_speed(partTypeEnemyDebris, 3, 4, -0.05, 0);