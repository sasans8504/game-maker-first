{
    "id": "68aa22f3-2393-4f14-aaec-b2226cc60d97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start",
    "eventList": [
        {
            "id": "85f4c901-e29b-485e-b6d0-381aeed921ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68aa22f3-2393-4f14-aaec-b2226cc60d97"
        },
        {
            "id": "b39cd489-19de-4465-99f6-25c8447474e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "68aa22f3-2393-4f14-aaec-b2226cc60d97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f00c354-4cb9-477b-80fd-478ea00de340",
    "visible": true
}