{
    "id": "bab36f59-3944-4dfe-97fc-60dc96c752ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laser",
    "eventList": [
        {
            "id": "f52afb72-c4f7-4dcf-ae9f-aa8e6e639f8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bab36f59-3944-4dfe-97fc-60dc96c752ba"
        },
        {
            "id": "c4b80ef1-712d-44cc-b97d-bee90d6b4fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bab36f59-3944-4dfe-97fc-60dc96c752ba"
        },
        {
            "id": "197f1b5a-7508-4bcc-bbbf-883dbe3e5de0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bab36f59-3944-4dfe-97fc-60dc96c752ba"
        },
        {
            "id": "6597e5a6-7b48-4356-a1c5-c762426da513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bab36f59-3944-4dfe-97fc-60dc96c752ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}