/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 6A7C4EA5
/// @DnDInput : 3
/// @DnDArgument : "var" "len"
/// @DnDArgument : "value" "500"
/// @DnDArgument : "var_1" "x2"
/// @DnDArgument : "value_1" "lengthdir_x(len, direction)"
/// @DnDArgument : "var_2" "y2"
/// @DnDArgument : "value_2" "lengthdir_y(len, direction)"
var len = 500;
var x2 = lengthdir_x(len, direction);
var y2 = lengthdir_y(len, direction);

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 4BECC6A4
/// @DnDArgument : "color" "image_blend"
draw_set_colour(image_blend & $ffffff);
var l4BECC6A4_0=(image_blend >> 24);
draw_set_alpha(l4BECC6A4_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Line
/// @DnDVersion : 1
/// @DnDHash : 069B260E
/// @DnDArgument : "x1_relative" "1"
/// @DnDArgument : "y1_relative" "1"
/// @DnDArgument : "x2" "x2"
/// @DnDArgument : "x2_relative" "1"
/// @DnDArgument : "y2" "y2"
/// @DnDArgument : "y2_relative" "1"
draw_line(x + 0, y + 0, x + x2, y + y2);

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 43CF4F5C
draw_set_colour($FFFFFFFF & $ffffff);
var l43CF4F5C_0=($FFFFFFFF >> 24);
draw_set_alpha(l43CF4F5C_0 / $ff);

/// @DnDAction : YoYo Games.Collisions.If_Collision_Shape
/// @DnDVersion : 1.1
/// @DnDHash : 1DDC0409
/// @DnDArgument : "x1_relative" "1"
/// @DnDArgument : "y1_relative" "1"
/// @DnDArgument : "x2" "x2"
/// @DnDArgument : "x2_relative" "1"
/// @DnDArgument : "y2" "y2"
/// @DnDArgument : "y2_relative" "1"
/// @DnDArgument : "target" "collisionList"
/// @DnDArgument : "target_temp" "1"
/// @DnDArgument : "obj" "obj_faction"
/// @DnDArgument : "aslist" "1"
/// @DnDSaveInfo : "obj" "bd10105f-8896-4994-92be-077c45253e8d"
var l1DDC0409_0 = ds_list_create();
var l1DDC0409_1 = collision_line_list(x + 0, y + 0, x + x2, y + y2, obj_faction, true, 1, l1DDC0409_0, true);
var collisionList = l1DDC0409_0;
if((l1DDC0409_1 > 0))
{
	/// @DnDAction : YoYo Games.Data Structures.List_Count
	/// @DnDVersion : 1
	/// @DnDHash : 608B3481
	/// @DnDParent : 1DDC0409
	/// @DnDArgument : "assignee" "count"
	/// @DnDArgument : "assignee_temp" "1"
	/// @DnDArgument : "var" "collisionList"
	var count = ds_list_size(collisionList);

	/// @DnDAction : YoYo Games.Loops.For_Loop
	/// @DnDVersion : 1
	/// @DnDHash : 1005E9D4
	/// @DnDParent : 1DDC0409
	/// @DnDArgument : "init_temp" "1"
	/// @DnDArgument : "cond" "i < count"
	for(var i = 0; i < count; i += 1) {
		/// @DnDAction : YoYo Games.Data Structures.List_Get_At
		/// @DnDVersion : 1
		/// @DnDHash : 44E4B437
		/// @DnDParent : 1005E9D4
		/// @DnDArgument : "assignee" "inst"
		/// @DnDArgument : "assignee_temp" "1"
		/// @DnDArgument : "var" "collisionList"
		/// @DnDArgument : "index" "i"
		var inst = ds_list_find_value(collisionList, i);
	
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0CCABDB8
		/// @DnDParent : 1005E9D4
		/// @DnDArgument : "var" "inst.immuneToLaser"
		/// @DnDArgument : "value" "false"
		if(inst.immuneToLaser == false)
		{
			/// @DnDAction : YoYo Games.Common.If_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 4F04A5A1
			/// @DnDParent : 0CCABDB8
			/// @DnDArgument : "var" "inst.faction"
			/// @DnDArgument : "not" "1"
			/// @DnDArgument : "value" "faction"
			if(!(inst.faction == faction))
			{
				/// @DnDAction : YoYo Games.Instances.Call_User_Event
				/// @DnDVersion : 1
				/// @DnDHash : 4B062FC5
				/// @DnDApplyTo : inst
				/// @DnDParent : 4F04A5A1
				/// @DnDArgument : "event" "1"
				with(inst) {
				event_user(1);
				}
			}
		}
	}
}