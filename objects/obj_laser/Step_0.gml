/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 79BBB851
/// @DnDArgument : "obj" "creator"
var l79BBB851_0 = false;
l79BBB851_0 = instance_exists(creator);
if(l79BBB851_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3C439498
	/// @DnDInput : 3
	/// @DnDParent : 79BBB851
	/// @DnDArgument : "expr" "creator.x"
	/// @DnDArgument : "expr_1" "creator.y"
	/// @DnDArgument : "expr_2" "creator.image_angle"
	/// @DnDArgument : "var" "x"
	/// @DnDArgument : "var_1" "y"
	/// @DnDArgument : "var_2" "direction"
	x = creator.x;
	y = creator.y;
	direction = creator.image_angle;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 4F2DC104
else
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0329E81D
	/// @DnDParent : 4F2DC104
	instance_destroy();
}