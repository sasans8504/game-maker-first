/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 78C9CDB0
/// @DnDArgument : "var" "other"
/// @DnDArgument : "not" "1"
/// @DnDArgument : "value" "creator"
if(!(other == creator))
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 252DCD2A
	/// @DnDParent : 78C9CDB0
	instance_destroy();

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2BCDCC82
	/// @DnDParent : 78C9CDB0
	/// @DnDArgument : "var" "other.faction"
	/// @DnDArgument : "not" "1"
	/// @DnDArgument : "value" "faction"
	if(!(other.faction == faction))
	{
		/// @DnDAction : YoYo Games.Instances.Call_User_Event
		/// @DnDVersion : 1
		/// @DnDHash : 42D9A519
		/// @DnDApplyTo : other
		/// @DnDParent : 2BCDCC82
		/// @DnDArgument : "event" "1"
		with(other) {
		event_user(1);
		}
	}
}