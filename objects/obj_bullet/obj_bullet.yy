{
    "id": "b2f3e12f-832a-465f-a232-32feeea761a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "0608da74-c891-43e6-b28f-2d00a8a90f3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2f3e12f-832a-465f-a232-32feeea761a1"
        },
        {
            "id": "47e1a8d4-bbcd-471d-bdec-60b524635916",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b2f3e12f-832a-465f-a232-32feeea761a1"
        },
        {
            "id": "22b1a10b-a9dc-41cb-8e7b-9ab90b79bba2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2f3e12f-832a-465f-a232-32feeea761a1"
        },
        {
            "id": "a861bda6-79e0-456c-8dc1-003284f71bd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "75d7e375-26c7-40d1-a3b4-c0ae707c1833",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b2f3e12f-832a-465f-a232-32feeea761a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3be64706-794f-4d50-b172-1ff105833f53",
    "visible": true
}