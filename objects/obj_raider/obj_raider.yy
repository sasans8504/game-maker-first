{
    "id": "ed8bdfd1-4449-4219-80b2-23568debcf15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_raider",
    "eventList": [
        {
            "id": "8cb7a9c4-f5bb-411b-9670-1321f31de339",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ed8bdfd1-4449-4219-80b2-23568debcf15"
        },
        {
            "id": "e715f53f-08c9-43ad-abec-c839c13f5f0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ed8bdfd1-4449-4219-80b2-23568debcf15"
        },
        {
            "id": "c3441600-d258-4f23-a7ca-a5a56ee07356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ed8bdfd1-4449-4219-80b2-23568debcf15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e3073e3d-55c4-4327-a93d-a5f731436a53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42cbee92-b0b8-48d1-aa26-005c32942ab6",
    "visible": true
}