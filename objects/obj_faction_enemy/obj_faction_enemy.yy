{
    "id": "e7da4ad1-efb6-4395-80fd-853b5f4c53d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_faction_enemy",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e85092fb-af1a-4f36-80c3-fcc2041d6a8c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "8e8e88b0-35d0-4ccd-a455-dcd023769085",
            "value": "$FF0000D1"
        },
        {
            "id": "901a61c9-19c5-4b1d-951f-cdd1772f72c2",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "faa90bfc-fe99-4d6b-bb32-bef2495249fc",
            "value": "\"enemy\""
        }
    ],
    "parentObjectId": "bd10105f-8896-4994-92be-077c45253e8d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}