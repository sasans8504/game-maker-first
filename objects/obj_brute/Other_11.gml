/// @DnDAction : YoYo Games.Instances.Inherit_Event
/// @DnDVersion : 1
/// @DnDHash : 3EE043F4
event_inherited();

/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 2A4DFC3A
/// @DnDArgument : "obj" "self"
/// @DnDArgument : "not" "1"
var l2A4DFC3A_0 = false;
l2A4DFC3A_0 = instance_exists(self);
if(!l2A4DFC3A_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2DC6C3D0
	/// @DnDApplyTo : 46fbed47-96ae-4a19-80e5-d989fb4d07b8
	/// @DnDParent : 2A4DFC3A
	/// @DnDArgument : "expr" "-1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "brute_num"
	with(obj_game) {
	brute_num += -1;
	
	}
}