{
    "id": "6ecdcc69-e8f0-4eef-9578-ed84dcc30fc3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_brute",
    "eventList": [
        {
            "id": "9fa4278e-25a3-4f06-9370-7c6c9bacaffc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ecdcc69-e8f0-4eef-9578-ed84dcc30fc3"
        },
        {
            "id": "116650c5-4ee9-4dd7-b930-06c6146dda5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ecdcc69-e8f0-4eef-9578-ed84dcc30fc3"
        },
        {
            "id": "65848ca3-e1a0-4a8a-928c-15e27bb8961f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6ecdcc69-e8f0-4eef-9578-ed84dcc30fc3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "23c42754-500a-4b6a-8ef7-e73a17b9f6a1",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "bd10105f-8896-4994-92be-077c45253e8d",
            "propertyId": "aeb6edff-854c-429e-a620-77ae7bd5453c",
            "value": "True"
        }
    ],
    "parentObjectId": "e3073e3d-55c4-4327-a93d-a5f731436a53",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
    "visible": true
}