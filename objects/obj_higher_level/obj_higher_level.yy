{
    "id": "eded4b13-3563-4d62-beac-2ab4b214ff27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_higher_level",
    "eventList": [
        {
            "id": "65923f8c-730d-4975-9f0c-1bece121b385",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eded4b13-3563-4d62-beac-2ab4b214ff27"
        },
        {
            "id": "3d592c4a-9f74-4563-9e29-0d80c106f26c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "eded4b13-3563-4d62-beac-2ab4b214ff27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "333b17d9-02cc-4b14-a0bc-7a6f68ab9867",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
    "visible": true
}