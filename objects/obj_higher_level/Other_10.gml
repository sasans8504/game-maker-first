/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1F4D23B3
/// @DnDApplyTo : 5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6
/// @DnDArgument : "var" "level"
/// @DnDArgument : "op" "1"
/// @DnDArgument : "value" "highest_level"
with(obj_level) var l1F4D23B3_0 = level < highest_level;
if(l1F4D23B3_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 30C2D95E
	/// @DnDApplyTo : 5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6
	/// @DnDParent : 1F4D23B3
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "level"
	with(obj_level) {
	level += 1;
	
	}
}