{
    "id": "5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level",
    "eventList": [
        {
            "id": "a4fb6bbd-2f64-4ceb-ad05-6b61922f03ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6"
        },
        {
            "id": "88b3dd9c-5226-451c-bdb8-a7e819bfcb41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6"
        },
        {
            "id": "3a132a7e-2ea5-4f1b-8d6f-20764d725b2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5fbca1ca-4bdf-48f9-b2f2-aca0194bf6a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f00c354-4cb9-477b-80fd-478ea00de340",
    "visible": true
}