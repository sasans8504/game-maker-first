/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 1BB3DEF1
/// @DnDArgument : "speed" "0"
image_speed = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 2DCF9992
/// @DnDArgument : "expr" "1"
/// @DnDArgument : "var" "image_index"
image_index = 1;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 1B73AB73
/// @DnDInput : 2
/// @DnDArgument : "expr_1" "7"
/// @DnDArgument : "var" "level"
/// @DnDArgument : "var_1" "highest_level"
level = 0;
highest_level = 7;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 6F0180F2
/// @DnDInput : 3
/// @DnDArgument : "var" "raider_num"
/// @DnDArgument : "var_1" "hunter_num"
/// @DnDArgument : "var_2" "brute_num"
raider_num = 0;
hunter_num = 0;
brute_num = 0;