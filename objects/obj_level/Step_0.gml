/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 17609B1B
/// @DnDArgument : "expr" "level"
var l17609B1B_0 = level;
switch(l17609B1B_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 48393A10
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "1"
	case 1:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 11258F5E
		/// @DnDInput : 3
		/// @DnDParent : 48393A10
		/// @DnDArgument : "expr" "5"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 5;
		hunter_num = 0;
		brute_num = 0;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 53B7816E
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "2"
	case 2:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 349DF494
		/// @DnDInput : 3
		/// @DnDParent : 53B7816E
		/// @DnDArgument : "expr" "15"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 15;
		hunter_num = 0;
		brute_num = 0;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 375442EC
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "3"
	case 3:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 08EABC61
		/// @DnDInput : 3
		/// @DnDParent : 375442EC
		/// @DnDArgument : "expr_1" "5"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 0;
		hunter_num = 5;
		brute_num = 0;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 32AFA94E
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "4"
	case 4:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3FF786CC
		/// @DnDInput : 3
		/// @DnDParent : 32AFA94E
		/// @DnDArgument : "expr" "10"
		/// @DnDArgument : "expr_1" "5"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 10;
		hunter_num = 5;
		brute_num = 0;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 3528C239
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "5"
	case 5:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 75149B89
		/// @DnDInput : 3
		/// @DnDParent : 3528C239
		/// @DnDArgument : "expr_2" "5"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 0;
		hunter_num = 0;
		brute_num = 5;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 14E1CEB2
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "6"
	case 6:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1BFC3A25
		/// @DnDInput : 3
		/// @DnDParent : 14E1CEB2
		/// @DnDArgument : "expr" "60"
		/// @DnDArgument : "expr_1" "60"
		/// @DnDArgument : "expr_2" "15"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 60;
		hunter_num = 60;
		brute_num = 15;
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 7C855D19
	/// @DnDParent : 17609B1B
	/// @DnDArgument : "const" "7"
	case 7:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 26312D24
		/// @DnDInput : 3
		/// @DnDParent : 7C855D19
		/// @DnDArgument : "expr" "200"
		/// @DnDArgument : "expr_1" "200"
		/// @DnDArgument : "expr_2" "200"
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 200;
		hunter_num = 200;
		brute_num = 200;
		break;

	/// @DnDAction : YoYo Games.Switch.Default
	/// @DnDVersion : 1
	/// @DnDHash : 70421121
	/// @DnDParent : 17609B1B
	default:
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 7F6DE1D8
		/// @DnDInput : 3
		/// @DnDParent : 70421121
		/// @DnDArgument : "var" "raider_num"
		/// @DnDArgument : "var_1" "hunter_num"
		/// @DnDArgument : "var_2" "brute_num"
		raider_num = 0;
		hunter_num = 0;
		brute_num = 0;
		break;
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3DF0877C
/// @DnDInput : 4
/// @DnDArgument : "expr" "raider_num"
/// @DnDArgument : "expr_1" "hunter_num"
/// @DnDArgument : "expr_2" "brute_num"
/// @DnDArgument : "expr_3" "level"
/// @DnDArgument : "var" "obj_game.raider_num"
/// @DnDArgument : "var_1" "obj_game.hunter_num"
/// @DnDArgument : "var_2" "obj_game.brute_num"
/// @DnDArgument : "var_3" "obj_game.level"
obj_game.raider_num = raider_num;
obj_game.hunter_num = hunter_num;
obj_game.brute_num = brute_num;
obj_game.level = level;