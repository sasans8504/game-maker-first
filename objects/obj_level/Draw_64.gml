/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 4EF3AC29
/// @DnDArgument : "halign" "fa_center"
/// @DnDArgument : "valign" "fa_middle"
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 61528E3C
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "xscale" "2"
/// @DnDArgument : "yscale" "2"
/// @DnDArgument : "caption" ""Level:""
/// @DnDArgument : "text" "level"
draw_text_transformed(x + 0, y + 0, string("Level:") + string(level), 2, 2, 0);

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 6459B44B
draw_set_halign(fa_left);
draw_set_valign(fa_top);

/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
/// @DnDVersion : 1
/// @DnDHash : 2ECAAC9B
/// @DnDArgument : "x" "-80"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "80"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "sprite" "spr_enemy_raider"
/// @DnDArgument : "col" "$FF0000FF"
/// @DnDSaveInfo : "sprite" "42cbee92-b0b8-48d1-aa26-005c32942ab6"
draw_sprite_ext(spr_enemy_raider, 0, x + -80, y + 80, 1, 1, 0, $FF0000FF & $ffffff, ($FF0000FF >> 24) / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 30BA7DD8
/// @DnDArgument : "x" "-40"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "60"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "xscale" "1.5"
/// @DnDArgument : "yscale" "1.5"
/// @DnDArgument : "caption" "" : ""
/// @DnDArgument : "text" "raider_num"
draw_text_transformed(x + -40, y + 60, string(" : ") + string(raider_num), 1.5, 1.5, 0);

/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
/// @DnDVersion : 1
/// @DnDHash : 3072B311
/// @DnDArgument : "x" "-80"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "140"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "sprite" "spr_enemy_hunter"
/// @DnDArgument : "col" "$FF0000FF"
/// @DnDSaveInfo : "sprite" "468b9ff8-a59d-4abb-93b3-65e9d9dc4ddf"
draw_sprite_ext(spr_enemy_hunter, 0, x + -80, y + 140, 1, 1, 0, $FF0000FF & $ffffff, ($FF0000FF >> 24) / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 5563AFC8
/// @DnDArgument : "x" "-40"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "120"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "xscale" "1.5"
/// @DnDArgument : "yscale" "1.5"
/// @DnDArgument : "caption" "" : ""
/// @DnDArgument : "text" "hunter_num"
draw_text_transformed(x + -40, y + 120, string(" : ") + string(hunter_num), 1.5, 1.5, 0);

/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
/// @DnDVersion : 1
/// @DnDHash : 477039D7
/// @DnDArgument : "x" "-80"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "220"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "sprite" "spr_enemy_brute_strip5"
/// @DnDArgument : "frame" "4"
/// @DnDArgument : "col" "$FF0000FF"
/// @DnDSaveInfo : "sprite" "e41f2e04-76ff-4512-936e-40f92f652400"
draw_sprite_ext(spr_enemy_brute_strip5, 4, x + -80, y + 220, 1, 1, 0, $FF0000FF & $ffffff, ($FF0000FF >> 24) / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 599F4FC9
/// @DnDArgument : "x" "-40"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "200"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "xscale" "1.5"
/// @DnDArgument : "yscale" "1.5"
/// @DnDArgument : "caption" "" : ""
/// @DnDArgument : "text" "brute_num"
draw_text_transformed(x + -40, y + 200, string(" : ") + string(brute_num), 1.5, 1.5, 0);