{
    "id": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29366197-97dc-42bc-b7d4-e2cea5af8153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "2bae8236-f72e-42dc-bec1-6a082633c08b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29366197-97dc-42bc-b7d4-e2cea5af8153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "406e9cc4-7894-4989-84dc-420c49018eb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29366197-97dc-42bc-b7d4-e2cea5af8153",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "bbd32485-65ca-4cd3-821b-d6cd7af7e7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "4f9fc2cd-d268-4f00-b498-dcf9b8d40769",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd32485-65ca-4cd3-821b-d6cd7af7e7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27ae5ae7-3086-4a08-bb25-36ca6f53cdba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd32485-65ca-4cd3-821b-d6cd7af7e7e8",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "4571f350-4852-470c-9064-8cef7c2b3659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "37e6fb6c-e48d-464d-880c-cdcde30c60c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4571f350-4852-470c-9064-8cef7c2b3659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1282e593-bc8c-4efe-9a36-1fb06187affb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4571f350-4852-470c-9064-8cef7c2b3659",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "90ba626f-99bb-4b8f-a726-738bbee6ae82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "3c6ba38b-f59a-46d0-808f-0c263df1c7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90ba626f-99bb-4b8f-a726-738bbee6ae82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ac1226-f76e-4685-9b0e-b58a440758e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90ba626f-99bb-4b8f-a726-738bbee6ae82",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "eb7bfae0-c4d7-4a8f-817a-9e1ee92c1e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "a9743a3c-2940-428d-99c9-227b2b1f6a8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7bfae0-c4d7-4a8f-817a-9e1ee92c1e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6deb89-1806-4028-865a-039caaa2c606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7bfae0-c4d7-4a8f-817a-9e1ee92c1e44",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "deaa5fef-91b2-4223-b37a-742dc03ec876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "3ddf8e7e-ea32-4d57-9042-820094d4ef34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deaa5fef-91b2-4223-b37a-742dc03ec876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a70f3b20-63ff-4fce-9f06-753c832f2ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deaa5fef-91b2-4223-b37a-742dc03ec876",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "f6261ffe-baf1-429c-8025-a39cefc12e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "e0036768-6792-4877-a1be-6ce69da9f8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6261ffe-baf1-429c-8025-a39cefc12e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7772f950-3bcd-497d-b7f4-8b170c23dae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6261ffe-baf1-429c-8025-a39cefc12e44",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "be21cf39-17bc-465c-9c7d-2f1dde51540f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "640028c2-424b-41ed-bbfe-0a2f946a4864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be21cf39-17bc-465c-9c7d-2f1dde51540f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1307854f-a0b2-44bd-bcd1-5181d1c9ae8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be21cf39-17bc-465c-9c7d-2f1dde51540f",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "c0217baa-6f65-4fa3-8851-4a14b2d8c2b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "3aa96c6a-aae0-4d2c-bc14-e7b0cf32ce86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0217baa-6f65-4fa3-8851-4a14b2d8c2b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a42dc97-d64d-46c9-9a4c-9e95ed81c44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0217baa-6f65-4fa3-8851-4a14b2d8c2b9",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "9edb3586-dc3b-4cb4-b42f-286bb06a4487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "ec00f132-9290-470a-b1a0-cec5685f7956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9edb3586-dc3b-4cb4-b42f-286bb06a4487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83552752-6400-441a-bd83-148a4ba04f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9edb3586-dc3b-4cb4-b42f-286bb06a4487",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "23dc2f93-5564-4925-89cc-ea7edb35f09c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "e0c65dc5-7d3d-4405-a7e6-0b12a81dd33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23dc2f93-5564-4925-89cc-ea7edb35f09c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f2e578-bf0e-49d9-921d-11153bfbe4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23dc2f93-5564-4925-89cc-ea7edb35f09c",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "39823e74-86a2-45a7-8ed0-1c22a44be850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "756a0aa8-7610-4052-b1bc-daf71a4841f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39823e74-86a2-45a7-8ed0-1c22a44be850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee91d0a5-7dab-46b3-b339-9b70ab1dea66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39823e74-86a2-45a7-8ed0-1c22a44be850",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "32ab31d0-616d-4909-ba47-b892553b9718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "6ddb60c1-1757-4ad4-93d2-433bbb50b4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32ab31d0-616d-4909-ba47-b892553b9718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9abffc3-0d07-403d-bb3b-5b9eb17cab7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32ab31d0-616d-4909-ba47-b892553b9718",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "5b8f8e31-1057-4578-a292-bc7d94fae390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "2081b030-4354-4a0b-9090-200b7b864bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8f8e31-1057-4578-a292-bc7d94fae390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b50c2a2-a42b-4d18-8705-e3645842b4ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8f8e31-1057-4578-a292-bc7d94fae390",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "4827031c-994f-447b-89f4-e55a5468695b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "b47ebfe1-08b0-4af4-9f1f-b1592e6bd2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4827031c-994f-447b-89f4-e55a5468695b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eea486e-ece3-41e3-97d6-2c3f35496f49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4827031c-994f-447b-89f4-e55a5468695b",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "115bb716-e633-4a6c-917d-95c55d709195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "aa5860dc-c8d7-474a-98bc-89955efe5a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115bb716-e633-4a6c-917d-95c55d709195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f43d30a-25ce-4d41-9430-fea0c091ccb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115bb716-e633-4a6c-917d-95c55d709195",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "1e8bf8c3-67a1-47d6-8bce-2356e96f35d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "bedee641-0b80-4e9c-bf70-fe12789faa28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8bf8c3-67a1-47d6-8bce-2356e96f35d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d24971b-0726-4858-95b5-85c06e22017c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8bf8c3-67a1-47d6-8bce-2356e96f35d4",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "2b891a22-9bba-433d-b0fa-5d14d5335e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "90c01ec0-6330-49fd-82c3-116228ea1797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b891a22-9bba-433d-b0fa-5d14d5335e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bd81dd-1506-4930-917b-54d67c5b4e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b891a22-9bba-433d-b0fa-5d14d5335e1b",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "d85948b4-9cb0-4924-b39f-dbb199685d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "b8afda6f-10cf-4d44-84f2-5ff28350ba50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85948b4-9cb0-4924-b39f-dbb199685d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8195f509-7515-476d-8b68-5d2df1eb69b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85948b4-9cb0-4924-b39f-dbb199685d48",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "14c3a9c2-08fe-4628-a541-cc5bb2e00de0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "1c833867-f355-4785-95de-819f4c025c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c3a9c2-08fe-4628-a541-cc5bb2e00de0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b28877a1-9fea-41f6-8e78-40040675e55e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c3a9c2-08fe-4628-a541-cc5bb2e00de0",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "2da2b13b-7553-4afc-ab8d-26463b32fc8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "5ada7fc8-cd87-4b83-a989-17045ef0a249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da2b13b-7553-4afc-ab8d-26463b32fc8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96fa5c00-ea06-4f54-a222-235ba2183c50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da2b13b-7553-4afc-ab8d-26463b32fc8a",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "8ef33065-e06f-4c02-802d-4708048a2e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "7617aa98-0403-4ca2-8306-01b22559ef6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef33065-e06f-4c02-802d-4708048a2e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a640ced3-1e78-4021-bf17-be8482a252ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef33065-e06f-4c02-802d-4708048a2e83",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "2c415161-041f-46fa-b800-3eebbb8558e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "e837fd40-d872-44d7-b298-54be7ddfeadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c415161-041f-46fa-b800-3eebbb8558e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13f146bc-128d-487c-87a0-1aa3725fabbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c415161-041f-46fa-b800-3eebbb8558e5",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "b6d96e73-a1dd-4725-8606-c82a829431bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "3628afc9-3b9b-475a-b1bd-6262805d2c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d96e73-a1dd-4725-8606-c82a829431bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d32d5c-d774-43fb-bf59-cd1320fd3d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d96e73-a1dd-4725-8606-c82a829431bf",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "b61fd9ee-0013-4fe3-8fce-dcc7957ba664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "149d5e82-ac64-4b1d-b17e-6e895cb7639a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61fd9ee-0013-4fe3-8fce-dcc7957ba664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af67b5e9-b741-4669-993c-55cee0e75188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61fd9ee-0013-4fe3-8fce-dcc7957ba664",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "f44204d3-68e6-4372-8398-0cfdab0a9935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "ec08792f-00a7-4212-bd76-3295f2e89d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44204d3-68e6-4372-8398-0cfdab0a9935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de3b7eb-d110-4e91-b506-c8e999fea91a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44204d3-68e6-4372-8398-0cfdab0a9935",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "6f98135e-a30b-495b-be12-c6a55c18e65c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "96103814-7f11-4968-9eb7-dd7945ce796c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f98135e-a30b-495b-be12-c6a55c18e65c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f99c6c-5373-48c0-b4ed-19d4d67374c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f98135e-a30b-495b-be12-c6a55c18e65c",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "7efb0747-5b99-41a7-b34d-21b4ab68aa4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "6848bdff-41be-442c-855d-f16b6ffb24fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efb0747-5b99-41a7-b34d-21b4ab68aa4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0e673b-5574-4ae4-9418-f2b9aab78bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efb0747-5b99-41a7-b34d-21b4ab68aa4f",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "64ef8418-9a58-4185-a6fd-c4f36c342634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "12b8adf1-c5c4-4e81-acf2-5fdbd9938426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ef8418-9a58-4185-a6fd-c4f36c342634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cd4382-ba15-450a-8151-2909f10afe9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ef8418-9a58-4185-a6fd-c4f36c342634",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "a32f352d-388e-4085-b2ea-dddf2af49a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "b6ebe6e0-fc3b-4624-8e98-9728d14e12dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32f352d-388e-4085-b2ea-dddf2af49a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6d2e7f-c3f3-4a8d-abea-ef3105efde60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32f352d-388e-4085-b2ea-dddf2af49a40",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "3be05339-ef2c-44f8-b409-d028d30e836e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "dec14e4e-963a-4518-82a8-8cb7d213fe6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be05339-ef2c-44f8-b409-d028d30e836e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2adc99d8-d3ae-4d7b-b605-c3e2b6a2ad55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be05339-ef2c-44f8-b409-d028d30e836e",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "ef19d528-795e-4bb0-a4b1-7972ff84c3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "910d1c43-0adb-4ee6-9c81-266bfa979432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef19d528-795e-4bb0-a4b1-7972ff84c3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc68f0d-8d0e-4d3f-988e-90c0a1271ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef19d528-795e-4bb0-a4b1-7972ff84c3bf",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "56cfee1d-d3e6-46d2-809a-766af6d7f105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "6a940fe0-f6db-43d9-a8ad-a4dd503d4667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cfee1d-d3e6-46d2-809a-766af6d7f105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699d3160-b24b-4c3a-ba45-bbbf7876d762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cfee1d-d3e6-46d2-809a-766af6d7f105",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "91c069c2-22cc-45ab-b745-7fff5b8c1a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "7541b182-f256-49e7-ae68-da3762dca021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c069c2-22cc-45ab-b745-7fff5b8c1a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394afb8d-3baf-41d6-92fb-840bde47d8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c069c2-22cc-45ab-b745-7fff5b8c1a05",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "a8c07912-e000-411c-b1ba-2bafc63c87bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "c2e5a67e-e65c-40dc-b6fc-43a9ea6bab5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8c07912-e000-411c-b1ba-2bafc63c87bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d84f19-d62e-49b2-b0e7-c2e51e78f655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8c07912-e000-411c-b1ba-2bafc63c87bf",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "56021d2e-e863-42bb-86a2-33dbee1597a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "0a9e3ae9-979a-4989-a1a1-07edd9e2b1bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56021d2e-e863-42bb-86a2-33dbee1597a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8e7c69-d367-4b67-b4ec-43d113db3743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56021d2e-e863-42bb-86a2-33dbee1597a3",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "4f73fd02-acfc-4f54-832d-29c8a5602cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "293012ad-9760-4467-9ab2-42c58e2a7c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f73fd02-acfc-4f54-832d-29c8a5602cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae372a3-3379-4320-8560-79303386fab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f73fd02-acfc-4f54-832d-29c8a5602cf7",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        },
        {
            "id": "135192a2-781a-4e6f-bec2-c3c7595ee790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "compositeImage": {
                "id": "fcaf3551-7b99-4426-b1c5-7a94a2e9055e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "135192a2-781a-4e6f-bec2-c3c7595ee790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb89480-2c42-4146-b40e-564e5dbb9ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "135192a2-781a-4e6f-bec2-c3c7595ee790",
                    "LayerId": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "d3a09d2f-ab3a-47fd-9734-2a89b88d4c62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdcde261-0447-4c76-ba8d-59b47b8f9ba6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": -22,
    "yorig": 60
}