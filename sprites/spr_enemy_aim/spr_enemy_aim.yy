{
    "id": "87d44bf7-8ee9-4e24-8983-d1de7b42e79b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_aim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "164db3a9-a2d0-4fc7-aa0a-0b725cdd1e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d44bf7-8ee9-4e24-8983-d1de7b42e79b",
            "compositeImage": {
                "id": "801fdf32-1a83-455e-a02a-7ce7c8e2b9c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164db3a9-a2d0-4fc7-aa0a-0b725cdd1e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92e28ddc-6ad4-45ca-9332-0d50506b6081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164db3a9-a2d0-4fc7-aa0a-0b725cdd1e3d",
                    "LayerId": "bb084204-fab1-4eb1-9106-2ec2167cbcb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bb084204-fab1-4eb1-9106-2ec2167cbcb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87d44bf7-8ee9-4e24-8983-d1de7b42e79b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}