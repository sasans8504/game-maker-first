{
    "id": "e299b911-a307-4b75-8876-de6a54442f6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_starfield_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecb27b3c-c5a8-4a77-8849-6d493318b12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e299b911-a307-4b75-8876-de6a54442f6a",
            "compositeImage": {
                "id": "e8ae0856-4898-4b4d-81b6-8d02591bbf17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb27b3c-c5a8-4a77-8849-6d493318b12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23adf7f6-0234-472c-8411-5335fd173234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb27b3c-c5a8-4a77-8849-6d493318b12f",
                    "LayerId": "2626eb4e-aed8-4362-ae59-40ec4cf9dc8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2626eb4e-aed8-4362-ae59-40ec4cf9dc8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e299b911-a307-4b75-8876-de6a54442f6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}