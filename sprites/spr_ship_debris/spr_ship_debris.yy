{
    "id": "80bdef99-292c-47e1-993a-6bf558ed988e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8e03809-66ff-4d02-8b02-f8e8739b4460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80bdef99-292c-47e1-993a-6bf558ed988e",
            "compositeImage": {
                "id": "0e5e45fa-7ec4-47fe-ad04-885b3fd5f3a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8e03809-66ff-4d02-8b02-f8e8739b4460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d6b6f9-483e-4a6f-b85e-ffc8d7502dd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8e03809-66ff-4d02-8b02-f8e8739b4460",
                    "LayerId": "e5930fd5-4353-4ccc-b8c7-c2c62a5544d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "e5930fd5-4353-4ccc-b8c7-c2c62a5544d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80bdef99-292c-47e1-993a-6bf558ed988e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}