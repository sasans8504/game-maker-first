{
    "id": "a53beb03-1604-45e4-9b45-25fdd121d3fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e2ebbf1-d0e0-460f-951a-34ad2fdf2402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a53beb03-1604-45e4-9b45-25fdd121d3fd",
            "compositeImage": {
                "id": "a927916f-5a97-431a-9c43-09cce8b50b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2ebbf1-d0e0-460f-951a-34ad2fdf2402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7292f643-f898-4140-ba5f-7b8ac121b3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2ebbf1-d0e0-460f-951a-34ad2fdf2402",
                    "LayerId": "820499de-46ff-4d53-88b4-a06dead508af"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "820499de-46ff-4d53-88b4-a06dead508af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a53beb03-1604-45e4-9b45-25fdd121d3fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}