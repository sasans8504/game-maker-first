{
    "id": "acb67e50-abae-4631-bae6-0342185eade9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_starfield_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 10,
    "bbox_right": 93,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fc625e8-3619-4cf2-bc53-9809c7541801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb67e50-abae-4631-bae6-0342185eade9",
            "compositeImage": {
                "id": "d0dd8967-0c11-4b4e-98d3-a941f61e00d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fc625e8-3619-4cf2-bc53-9809c7541801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db016def-7320-48d1-9cd2-b7d0ef98058a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fc625e8-3619-4cf2-bc53-9809c7541801",
                    "LayerId": "5ecd9fd8-0ffe-4288-b25c-6e058ab72cc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "5ecd9fd8-0ffe-4288-b25c-6e058ab72cc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acb67e50-abae-4631-bae6-0342185eade9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}