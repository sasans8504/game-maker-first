{
    "id": "d2cf9aae-fa3b-4298-a36c-fca02ddd94d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dc6015f-62b8-42a4-846a-04e3ed7b45ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2cf9aae-fa3b-4298-a36c-fca02ddd94d6",
            "compositeImage": {
                "id": "71518add-4fdf-4f53-82b4-8bf2cf6b425d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc6015f-62b8-42a4-846a-04e3ed7b45ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b02485-02c1-4893-815c-3de3ec681894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc6015f-62b8-42a4-846a-04e3ed7b45ab",
                    "LayerId": "0833b4b3-14b7-419f-8a59-315bb14c89d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0833b4b3-14b7-419f-8a59-315bb14c89d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2cf9aae-fa3b-4298-a36c-fca02ddd94d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}