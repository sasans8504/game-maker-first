{
    "id": "3a6ccdb4-5d1c-4c1a-9a63-3ce139421e2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_star_gradient",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 498,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f35e6a5-4c63-482c-b934-d7f6c9c4bcab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a6ccdb4-5d1c-4c1a-9a63-3ce139421e2f",
            "compositeImage": {
                "id": "a89eb6d9-eae2-4af1-b06a-819dc9bd58d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f35e6a5-4c63-482c-b934-d7f6c9c4bcab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b8c06e-ffb5-48a6-a3a5-925866c90af1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f35e6a5-4c63-482c-b934-d7f6c9c4bcab",
                    "LayerId": "5f6ad22b-0bb5-47c8-af47-edf21fa7dec3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "5f6ad22b-0bb5-47c8-af47-edf21fa7dec3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a6ccdb4-5d1c-4c1a-9a63-3ce139421e2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}