{
    "id": "5f00c354-4cb9-477b-80fd-478ea00de340",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 8,
    "bbox_right": 247,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f83dfe43-3046-44e0-addb-a7abd7871a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f00c354-4cb9-477b-80fd-478ea00de340",
            "compositeImage": {
                "id": "de98e0f9-cd44-482c-af8b-7fb0ac2f095b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83dfe43-3046-44e0-addb-a7abd7871a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2090cb3-8157-4b64-8673-36c318759f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83dfe43-3046-44e0-addb-a7abd7871a18",
                    "LayerId": "18173232-b7d3-4ab5-9c01-148a0d2aa26f"
                }
            ]
        },
        {
            "id": "9803109f-7d15-40ce-93fd-58d5fc920ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f00c354-4cb9-477b-80fd-478ea00de340",
            "compositeImage": {
                "id": "0bb2f843-1ab3-4949-bab2-0205bd339aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9803109f-7d15-40ce-93fd-58d5fc920ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5894f3a9-6d87-474e-a1cf-63574ad5fe31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9803109f-7d15-40ce-93fd-58d5fc920ed4",
                    "LayerId": "18173232-b7d3-4ab5-9c01-148a0d2aa26f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "18173232-b7d3-4ab5-9c01-148a0d2aa26f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f00c354-4cb9-477b-80fd-478ea00de340",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}