{
    "id": "e41f2e04-76ff-4512-936e-40f92f652400",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_brute_strip5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9e376a7-a2f2-4015-abff-f26b1a8c94bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "compositeImage": {
                "id": "236dda81-d8ab-45c4-94f1-0f89025f5a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e376a7-a2f2-4015-abff-f26b1a8c94bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df41f7d5-ccfc-4575-bc9a-bf610058cc5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e376a7-a2f2-4015-abff-f26b1a8c94bb",
                    "LayerId": "919d9293-4852-4865-bb98-d8a39238c3d6"
                }
            ]
        },
        {
            "id": "955f5551-2bc7-4aa3-af06-0f715e605074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "compositeImage": {
                "id": "5a1e33e1-b112-41a3-943f-10b70258c6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955f5551-2bc7-4aa3-af06-0f715e605074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f935fd4-a167-4bd8-87db-3fb571cb1407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955f5551-2bc7-4aa3-af06-0f715e605074",
                    "LayerId": "919d9293-4852-4865-bb98-d8a39238c3d6"
                }
            ]
        },
        {
            "id": "fb0bf35d-5bff-4c87-a371-980b3749a3ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "compositeImage": {
                "id": "83e241e5-1e05-4bec-aba0-a0a9444c74fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb0bf35d-5bff-4c87-a371-980b3749a3ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c055eb0b-440d-4c08-b42a-0c64e7bade36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb0bf35d-5bff-4c87-a371-980b3749a3ee",
                    "LayerId": "919d9293-4852-4865-bb98-d8a39238c3d6"
                }
            ]
        },
        {
            "id": "bf8b7ad5-4472-4789-a344-ee8076ad2998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "compositeImage": {
                "id": "740eca5e-b9bc-4474-b231-3f5164306593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf8b7ad5-4472-4789-a344-ee8076ad2998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b89addb-f144-4ea4-8f07-7ca4bc7a9614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf8b7ad5-4472-4789-a344-ee8076ad2998",
                    "LayerId": "919d9293-4852-4865-bb98-d8a39238c3d6"
                }
            ]
        },
        {
            "id": "ab80e73f-00e3-4d6f-a975-fb3070a845e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "compositeImage": {
                "id": "373a53e1-f42b-4885-adf6-e8d02d13b93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab80e73f-00e3-4d6f-a975-fb3070a845e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5819c58c-ff80-43b7-a29d-79fc0f66f679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab80e73f-00e3-4d6f-a975-fb3070a845e9",
                    "LayerId": "919d9293-4852-4865-bb98-d8a39238c3d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "919d9293-4852-4865-bb98-d8a39238c3d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e41f2e04-76ff-4512-936e-40f92f652400",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}