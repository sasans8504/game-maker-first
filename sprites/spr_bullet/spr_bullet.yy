{
    "id": "3be64706-794f-4d50-b172-1ff105833f53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d654c40b-cfe9-447b-abf7-a11acb75619e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be64706-794f-4d50-b172-1ff105833f53",
            "compositeImage": {
                "id": "dc1d8bb7-3412-4ee4-949d-2cc7764311b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d654c40b-cfe9-447b-abf7-a11acb75619e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7f08d4-377c-4dda-8a72-6c71f5b269cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d654c40b-cfe9-447b-abf7-a11acb75619e",
                    "LayerId": "d3b385fc-39fc-44a3-a1a0-b6bb8d1556a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "d3b385fc-39fc-44a3-a1a0-b6bb8d1556a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3be64706-794f-4d50-b172-1ff105833f53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}