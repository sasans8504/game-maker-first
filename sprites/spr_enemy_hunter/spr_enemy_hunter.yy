{
    "id": "468b9ff8-a59d-4abb-93b3-65e9d9dc4ddf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_hunter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96b26cf2-f869-42dd-947c-dcc4b6d638f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468b9ff8-a59d-4abb-93b3-65e9d9dc4ddf",
            "compositeImage": {
                "id": "5e0ef627-d06b-4283-a396-e71c3106091a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b26cf2-f869-42dd-947c-dcc4b6d638f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b70dae-e3bf-4acf-86d5-568380ddb8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b26cf2-f869-42dd-947c-dcc4b6d638f0",
                    "LayerId": "fbe075c5-1183-41b9-945c-ad2bf9db6e64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbe075c5-1183-41b9-945c-ad2bf9db6e64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "468b9ff8-a59d-4abb-93b3-65e9d9dc4ddf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}