{
    "id": "9f36a510-8f37-4eb0-ac49-4133e02d5ea2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_starfield_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74484142-b24a-4989-b9a0-aed3471267b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f36a510-8f37-4eb0-ac49-4133e02d5ea2",
            "compositeImage": {
                "id": "c6d9c28f-cd84-4dfc-a1cb-6a1c3392851f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74484142-b24a-4989-b9a0-aed3471267b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f87596d-4fa8-428c-933f-9427394a14c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74484142-b24a-4989-b9a0-aed3471267b0",
                    "LayerId": "906d6480-9881-4a2e-b6b5-ea0cb9a5760d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "906d6480-9881-4a2e-b6b5-ea0cb9a5760d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f36a510-8f37-4eb0-ac49-4133e02d5ea2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}