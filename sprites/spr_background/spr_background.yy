{
    "id": "78ac71ff-6e62-4998-9775-57f198b3b482",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74220e40-7b97-48a1-a596-2dc7bb741cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78ac71ff-6e62-4998-9775-57f198b3b482",
            "compositeImage": {
                "id": "91b0ef67-bf2e-43e4-9ce2-8ebae47e5486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74220e40-7b97-48a1-a596-2dc7bb741cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b66a405-faa9-4ad4-919f-27c19d0d9dcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74220e40-7b97-48a1-a596-2dc7bb741cc4",
                    "LayerId": "2f2fd489-dc33-4ab4-b330-f93ee7a3d42b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2f2fd489-dc33-4ab4-b330-f93ee7a3d42b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78ac71ff-6e62-4998-9775-57f198b3b482",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}