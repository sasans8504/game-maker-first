{
    "id": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_level",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de6298e-de7c-437b-ac5f-c255742bc4f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
            "compositeImage": {
                "id": "ff58e491-cce1-45f6-afca-bc6c0eee71dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de6298e-de7c-437b-ac5f-c255742bc4f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e2d7212-42f7-4174-b669-0e751fe9be4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de6298e-de7c-437b-ac5f-c255742bc4f4",
                    "LayerId": "40e528f7-ffb5-4d14-b98d-a19f2fc15728"
                }
            ]
        },
        {
            "id": "a872e812-f538-4c19-8b60-fcfba2dfbb62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
            "compositeImage": {
                "id": "b4b04369-97b1-41ee-b36c-c58904bc23cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a872e812-f538-4c19-8b60-fcfba2dfbb62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfef3699-da13-46e2-9756-8f7e7086bb43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a872e812-f538-4c19-8b60-fcfba2dfbb62",
                    "LayerId": "40e528f7-ffb5-4d14-b98d-a19f2fc15728"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 64,
    "layers": [
        {
            "id": "40e528f7-ffb5-4d14-b98d-a19f2fc15728",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa253556-e6cf-45dc-9c94-a7e783e0f281",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}