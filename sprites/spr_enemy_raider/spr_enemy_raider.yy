{
    "id": "42cbee92-b0b8-48d1-aa26-005c32942ab6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_raider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88e67c57-335a-4d04-9357-d36cbe111dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42cbee92-b0b8-48d1-aa26-005c32942ab6",
            "compositeImage": {
                "id": "829210ab-4067-423c-ba1d-948573b7e818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e67c57-335a-4d04-9357-d36cbe111dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a920f6f0-4c1e-4d77-8184-f9ac86fbc77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e67c57-335a-4d04-9357-d36cbe111dc2",
                    "LayerId": "e824cd52-7e37-4f47-865a-1d5ec2b465f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e824cd52-7e37-4f47-865a-1d5ec2b465f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42cbee92-b0b8-48d1-aa26-005c32942ab6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}