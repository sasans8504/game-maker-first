{
    "id": "6d7e1ca7-f946-4a27-807f-c021b29e7166",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 3,
    "bbox_right": 54,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4ba956d-abc7-4ba6-8e58-fe3fa5b5fea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7e1ca7-f946-4a27-807f-c021b29e7166",
            "compositeImage": {
                "id": "feb0abee-f750-4037-9bf6-7c283a25c9d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ba956d-abc7-4ba6-8e58-fe3fa5b5fea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2614713e-5388-4b93-9471-58d65ab049be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ba956d-abc7-4ba6-8e58-fe3fa5b5fea6",
                    "LayerId": "81fcc78e-525d-40dc-a797-03538aa9c94e"
                }
            ]
        },
        {
            "id": "6aa18fc9-3df2-48a9-a91d-72120232e018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7e1ca7-f946-4a27-807f-c021b29e7166",
            "compositeImage": {
                "id": "099be357-4d32-4023-9fde-4aca4f274f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aa18fc9-3df2-48a9-a91d-72120232e018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca9106e-2260-42cc-9a05-c9c51973e8e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aa18fc9-3df2-48a9-a91d-72120232e018",
                    "LayerId": "81fcc78e-525d-40dc-a797-03538aa9c94e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "81fcc78e-525d-40dc-a797-03538aa9c94e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d7e1ca7-f946-4a27-807f-c021b29e7166",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}