{
    "id": "3fe01f8b-3690-4ef6-be06-a7a3fa6eafbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7625f25d-d63b-478f-8115-048f1132191a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fe01f8b-3690-4ef6-be06-a7a3fa6eafbd",
            "compositeImage": {
                "id": "ac922c21-1b0d-4802-924e-874bc603edee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7625f25d-d63b-478f-8115-048f1132191a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e107afa2-cce0-43a8-8386-d654b7a05ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7625f25d-d63b-478f-8115-048f1132191a",
                    "LayerId": "744130e6-b7e2-4483-a52c-96ca499ce6bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "744130e6-b7e2-4483-a52c-96ca499ce6bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fe01f8b-3690-4ef6-be06-a7a3fa6eafbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}