{
    "id": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powerup_strip7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e753a32-920c-445b-8f85-647b788c9b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "7c1631e2-c342-46ff-8fd5-ac4845e275d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e753a32-920c-445b-8f85-647b788c9b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89f096b-1a62-487c-b0e0-74105c59bab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e753a32-920c-445b-8f85-647b788c9b5f",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "dfb9ab53-c3df-4fd1-83c9-97796a46267e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "2c7e9a1b-ec73-48ba-99c2-c57b7ce37803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb9ab53-c3df-4fd1-83c9-97796a46267e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "739b12e3-7b55-4864-925f-24f9ce266425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb9ab53-c3df-4fd1-83c9-97796a46267e",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "4d6aa3f4-5c5d-4643-b59d-4a4e30313317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "9061516b-0e93-4083-8c2a-2e455ca76f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6aa3f4-5c5d-4643-b59d-4a4e30313317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2393d49-38ee-4a02-b522-b8f683c2b003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6aa3f4-5c5d-4643-b59d-4a4e30313317",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "06cac9c8-0a7d-49f4-a250-f79e5c32105e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "7f23916b-0749-4b89-9930-a04c778e39d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06cac9c8-0a7d-49f4-a250-f79e5c32105e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0e5123-730a-4aa3-b59b-5a05422a4a16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06cac9c8-0a7d-49f4-a250-f79e5c32105e",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "70ac7db0-7bbb-4e91-864a-ea9fbf2c32a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "6410d1d3-10f0-4ce1-8c6c-749675d1b830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ac7db0-7bbb-4e91-864a-ea9fbf2c32a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8cc1cd-ae7d-47d2-ac78-8ed3b2c09e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ac7db0-7bbb-4e91-864a-ea9fbf2c32a4",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "88653a66-b90d-48bd-9f56-e8e4c4e0e4f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "a82226d7-77e1-4a76-ae6a-bb12aa920b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88653a66-b90d-48bd-9f56-e8e4c4e0e4f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b213ee4e-816b-4377-afe0-cfb68b7f96e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88653a66-b90d-48bd-9f56-e8e4c4e0e4f1",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        },
        {
            "id": "7bae1ad0-d763-4f18-99b4-da5c6ac840a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "compositeImage": {
                "id": "c965e82b-ff24-42d6-865e-b6b56f777dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bae1ad0-d763-4f18-99b4-da5c6ac840a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c02d88eb-4239-448f-b029-274d3ebe1744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bae1ad0-d763-4f18-99b4-da5c6ac840a2",
                    "LayerId": "ee85861f-24bb-4a5d-a2b3-17103a60e342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ee85861f-24bb-4a5d-a2b3-17103a60e342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54115d35-2358-48f8-9e5f-fbd4357b9f67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}