{
    "id": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08af4915-13f5-42bb-a1ec-2db24b70cdd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "c75c6139-13d1-495a-8982-992e08072f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08af4915-13f5-42bb-a1ec-2db24b70cdd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8771ba3-efa9-4e7e-b344-80c7c97cc894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08af4915-13f5-42bb-a1ec-2db24b70cdd9",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        },
        {
            "id": "d360c7eb-86e4-4d87-a59e-92425498ec5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "47db370a-0d0e-4485-8fa6-3bff43c645d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d360c7eb-86e4-4d87-a59e-92425498ec5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8109d45-b1ba-44c8-914c-8c085da4d2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d360c7eb-86e4-4d87-a59e-92425498ec5a",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        },
        {
            "id": "e3c991c5-3d73-455e-825c-369d0d9d3f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "0d9532c7-6069-414d-bc18-e7d08cc56f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c991c5-3d73-455e-825c-369d0d9d3f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ff0bc5-b480-42c4-9c08-4c8da2d47b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c991c5-3d73-455e-825c-369d0d9d3f67",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        },
        {
            "id": "5cb3a3fd-8965-4c22-9ae8-cb47033dadfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "e34023c6-d805-46d6-ad7d-4d75f7875d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cb3a3fd-8965-4c22-9ae8-cb47033dadfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62cc0a0-c8fe-486e-bddd-ab3d5b226a53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cb3a3fd-8965-4c22-9ae8-cb47033dadfa",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        },
        {
            "id": "33baf9b3-d926-4fa0-b520-a76239e45548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "20dd77e3-6449-4a4b-8f22-ae37a6f449db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33baf9b3-d926-4fa0-b520-a76239e45548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec266821-fbf6-48c0-94bd-983bee32d403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33baf9b3-d926-4fa0-b520-a76239e45548",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        },
        {
            "id": "f74b28fa-d9d1-43f8-b195-921b7212efba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "compositeImage": {
                "id": "c9378fba-416f-489a-941d-f84beb7c1b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74b28fa-d9d1-43f8-b195-921b7212efba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7511328-30bc-4543-abb3-cdcb93005dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74b28fa-d9d1-43f8-b195-921b7212efba",
                    "LayerId": "33d01016-0e84-4d2b-b91d-91975b8e5975"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "33d01016-0e84-4d2b-b91d-91975b8e5975",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2aec178-e6cf-4f42-b4ca-64e08ad870d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}