{
    "id": "f1e69fdd-b447-4daf-9a61-1c3404bb3431",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exhaust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bcbaaa1-8684-48a2-a286-f190b7b2725d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1e69fdd-b447-4daf-9a61-1c3404bb3431",
            "compositeImage": {
                "id": "6c329a57-d810-4568-93fc-8cd72be2cdf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bcbaaa1-8684-48a2-a286-f190b7b2725d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0096fcd4-9dfb-41bd-9ece-6668bdb6ff46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bcbaaa1-8684-48a2-a286-f190b7b2725d",
                    "LayerId": "71dcd1bb-385e-4594-a415-b7a73c23ef3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "71dcd1bb-385e-4594-a415-b7a73c23ef3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1e69fdd-b447-4daf-9a61-1c3404bb3431",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}