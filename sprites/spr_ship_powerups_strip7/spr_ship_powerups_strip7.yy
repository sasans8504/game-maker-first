{
    "id": "9fecd960-4fbf-4a7c-b15e-5185866821be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship_powerups_strip7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 5,
    "bbox_right": 45,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6204a9d8-617c-4184-b40d-3af3ae778ece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "c5ee87d8-c961-4382-8218-027001350317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6204a9d8-617c-4184-b40d-3af3ae778ece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "140e80c1-5ff2-4390-ae77-3c2274e93897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6204a9d8-617c-4184-b40d-3af3ae778ece",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "f0cf3ef7-fcb6-42c8-8a4b-1442ec78ba8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "7f8eb666-c64f-4eba-b7d5-a2aa0d4a625c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cf3ef7-fcb6-42c8-8a4b-1442ec78ba8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f69f86b-58cf-4951-958f-ee0c22512919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cf3ef7-fcb6-42c8-8a4b-1442ec78ba8d",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "ad0121d4-56f4-4424-8a8f-b278a9b23345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "1b7faeb2-eecd-4af3-8038-ebc08d1d2864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0121d4-56f4-4424-8a8f-b278a9b23345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2527da2f-6954-4475-abc5-f2b2e40d9120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0121d4-56f4-4424-8a8f-b278a9b23345",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "8aa74231-e491-4ac4-a7c8-3c54e16a9970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "9812f9fd-3d52-4ee8-9486-a1c8bc65bd4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa74231-e491-4ac4-a7c8-3c54e16a9970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc39d702-9c5f-4289-bbd1-027a3825cf46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa74231-e491-4ac4-a7c8-3c54e16a9970",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "fe8500a8-9f58-4f43-85e7-be0d70dc0dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "420ca641-db14-4fbb-a2d4-d9d9c3a5e102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8500a8-9f58-4f43-85e7-be0d70dc0dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad9a882-5ee0-4854-a036-e80cbb6b7d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8500a8-9f58-4f43-85e7-be0d70dc0dbf",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "14ab600f-719b-4f96-94da-901c44fbf4ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "8cbc39f3-a9fb-4615-a108-deb501c66190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ab600f-719b-4f96-94da-901c44fbf4ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1b4146-f56d-43e0-83f6-caab6de4e941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ab600f-719b-4f96-94da-901c44fbf4ed",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        },
        {
            "id": "6f6d6cf8-6fd6-4391-a44f-cb53fa3b3d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "compositeImage": {
                "id": "1680b96d-7c3b-4d10-93b3-66ed4d24a7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6d6cf8-6fd6-4391-a44f-cb53fa3b3d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e2f061-e078-40f1-b6ed-f7dd8f9f33fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6d6cf8-6fd6-4391-a44f-cb53fa3b3d1b",
                    "LayerId": "eccf89f6-4ea3-4525-9724-5a887d098136"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "eccf89f6-4ea3-4525-9724-5a887d098136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fecd960-4fbf-4a7c-b15e-5185866821be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}