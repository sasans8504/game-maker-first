{
    "id": "3beb6ac2-8816-483a-a591-d7322ff8f339",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "095436ab-fd78-4e71-8209-a0922bf3999a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3beb6ac2-8816-483a-a591-d7322ff8f339",
            "compositeImage": {
                "id": "26db10ee-70f5-45ed-9f10-275a006462a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095436ab-fd78-4e71-8209-a0922bf3999a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "957ffe28-f4b1-42c7-a314-93fb18d24428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095436ab-fd78-4e71-8209-a0922bf3999a",
                    "LayerId": "d9e33b3a-b0f6-4ea0-948a-0586d80cc512"
                }
            ]
        }
    ],
    "gridX": 6,
    "gridY": 6,
    "height": 24,
    "layers": [
        {
            "id": "d9e33b3a-b0f6-4ea0-948a-0586d80cc512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3beb6ac2-8816-483a-a591-d7322ff8f339",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}