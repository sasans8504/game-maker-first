{
    "id": "ccf20e0d-8cf0-4f2f-aea0-cd396b7eec83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc5cfa67-c498-44a2-8874-397a598af1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf20e0d-8cf0-4f2f-aea0-cd396b7eec83",
            "compositeImage": {
                "id": "77439364-378f-4a5c-8243-cd06dc7515d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5cfa67-c498-44a2-8874-397a598af1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57a4d85-0b84-47bb-b657-bff86649251a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5cfa67-c498-44a2-8874-397a598af1a9",
                    "LayerId": "54e277c9-47df-4871-82cb-f581feddc23e"
                }
            ]
        },
        {
            "id": "b75a337a-5dc0-4c2f-9f08-c3c88d7178b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf20e0d-8cf0-4f2f-aea0-cd396b7eec83",
            "compositeImage": {
                "id": "8c63f22c-89eb-4b11-838d-564e027c93b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75a337a-5dc0-4c2f-9f08-c3c88d7178b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909dc62f-2dea-443f-9f23-6162014283ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75a337a-5dc0-4c2f-9f08-c3c88d7178b1",
                    "LayerId": "54e277c9-47df-4871-82cb-f581feddc23e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54e277c9-47df-4871-82cb-f581feddc23e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccf20e0d-8cf0-4f2f-aea0-cd396b7eec83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}