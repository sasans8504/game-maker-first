Navigation
bullet effect setting:script-create_bullet
create powerup:obj_powerup-Create
mouse lock inside:obj_game-Step
win or lose point:obj_game-Step
enemy count:obj_game-Create, obj_game-Room Start

Future Update:
//增加互動教學關卡
增加敵機提供的分數
V 增加一個選擇難度的畫面(包含該關卡敵機種類/數量)
增加敵機剩餘數量的顯示
勝利改為擊敗所有敵機
敵機小地圖
死亡改為原地復活
增加一個powerup switch(召喚兩台子機)
	
教學關+第一關 必定生成一個powerup在面前
	5個雷達
第二關
	15個雷達
第三關
	5個敵機
第四關
	10個雷達+5個敵機
第五關
	5個戰艦
第六關
	5個雷達+5個戰艦
第七關
	5個雷達+5個敵機+5個戰艦
第八關
	10/10/10
第九關
	20/20/20
瘋狂難度 + 必定power up
	60/40/25
地獄難度 + 必定power up
	80/60/40