/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 4A36DB70
/// @DnDInput : 6
/// @DnDArgument : "var" "dir"
/// @DnDArgument : "value" "argument0"
/// @DnDArgument : "var_1" "spd"
/// @DnDArgument : "value_1" "argument1"
/// @DnDArgument : "var_2" "fac"
/// @DnDArgument : "value_2" "faction"
/// @DnDArgument : "var_3" "col"
/// @DnDArgument : "value_3" "image_blend"
/// @DnDArgument : "var_4" "creator_id"
/// @DnDArgument : "value_4" "id"
/// @DnDArgument : "var_5" "newBullet"
/// @DnDArgument : "value_5" "argument2"
var dir = argument0;
var spd = argument1;
var fac = faction;
var col = image_blend;
var creator_id = id;
var newBullet = argument2;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 17AF8FDC
/// @DnDInput : 5
/// @DnDApplyTo : newBullet
/// @DnDArgument : "expr" "dir"
/// @DnDArgument : "expr_1" "spd"
/// @DnDArgument : "expr_2" "fac"
/// @DnDArgument : "expr_3" "col"
/// @DnDArgument : "expr_4" "creator_id"
/// @DnDArgument : "var" "direction"
/// @DnDArgument : "var_1" "speed"
/// @DnDArgument : "var_2" "faction"
/// @DnDArgument : "var_3" "image_blend"
/// @DnDArgument : "var_4" "creator"
with(newBullet) {
direction = dir;
speed = spd;
faction = fac;
image_blend = col;
creator = creator_id;

}