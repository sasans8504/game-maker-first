/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 1AB06623
/// @DnDArgument : "expr" "3"
/// @DnDArgument : "var" "exhaustDensity"
exhaustDensity = 3;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7FF1922B
/// @DnDArgument : "var" "argument_count"
/// @DnDArgument : "op" "2"
if(argument_count > 0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4438D0C6
	/// @DnDParent : 7FF1922B
	/// @DnDArgument : "expr" "argument0"
	/// @DnDArgument : "var" "exhaustDensity"
	exhaustDensity = argument0;
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 444D52CA
/// @DnDArgument : "expr" "1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "exhaustCounter"
exhaustCounter += 1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6164A902
/// @DnDArgument : "var" "exhaustCounter"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "3"
if(exhaustCounter > 3)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 360B9CB6
	/// @DnDParent : 6164A902
	/// @DnDArgument : "var" "exhaustCounter"
	exhaustCounter = 0;

	/// @DnDAction : YoYo Games.Common.Temp_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 520415E8
	/// @DnDParent : 6164A902
	/// @DnDArgument : "var" "len"
	/// @DnDArgument : "value" "sprite_height * .4"
	var len = sprite_height * .4;

	/// @DnDAction : YoYo Games.Common.Temp_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4CD71257
	/// @DnDInput : 2
	/// @DnDParent : 6164A902
	/// @DnDArgument : "var" "xx"
	/// @DnDArgument : "value" "x + lengthdir_x(len, image_angle - 180)"
	/// @DnDArgument : "var_1" "yy"
	/// @DnDArgument : "value_1" "y + lengthdir_y(len, image_angle - 180)"
	var xx = x + lengthdir_x(len, image_angle - 180);
	var yy = y + lengthdir_y(len, image_angle - 180);

	/// @DnDAction : YoYo Games.Particles.Part_Particles_Create
	/// @DnDVersion : 1
	/// @DnDHash : 1A09C1DF
	/// @DnDApplyTo : 749f9bc2-29d1-48cc-b89e-10bf2ae8a9b3
	/// @DnDParent : 6164A902
	/// @DnDArgument : "x" "xx"
	/// @DnDArgument : "y" "yy"
	/// @DnDArgument : "system" "partSys"
	/// @DnDArgument : "type" "partTypeExhaust"
	/// @DnDArgument : "number" "1"
	with(obj_particles) part_particles_create(partSys, xx, yy, partTypeExhaust, 1);
}