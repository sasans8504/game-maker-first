/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 7C9EC16C
/// @DnDInput : 3
/// @DnDArgument : "var" "obj"
/// @DnDArgument : "value" "argument0"
/// @DnDArgument : "var_1" "number"
/// @DnDArgument : "value_1" "argument1"
/// @DnDArgument : "var_2" "pad"
/// @DnDArgument : "value_2" "64"
var obj = argument0;
var number = argument1;
var pad = 64;

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 07100AFE
/// @DnDArgument : "times" "number"
repeat(number)
{
	/// @DnDAction : YoYo Games.Common.Temp_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 5D66F390
	/// @DnDParent : 07100AFE
	/// @DnDArgument : "var" "boundCheck"
	/// @DnDArgument : "value" "true"
	var boundCheck = true;

	/// @DnDAction : YoYo Games.Loops.While_Loop
	/// @DnDVersion : 1
	/// @DnDHash : 6BE47D49
	/// @DnDParent : 07100AFE
	/// @DnDArgument : "var" "boundCheck"
	/// @DnDArgument : "value" "true"
	while ((boundCheck == true)) {
		/// @DnDAction : YoYo Games.Random.Get_Random_Number
		/// @DnDVersion : 1
		/// @DnDHash : 1FFDDD8A
		/// @DnDParent : 6BE47D49
		/// @DnDArgument : "var" "xx"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "max" "room_width"
		var xx = (random_range(0, room_width));
	
		/// @DnDAction : YoYo Games.Random.Get_Random_Number
		/// @DnDVersion : 1
		/// @DnDHash : 081DA288
		/// @DnDParent : 6BE47D49
		/// @DnDArgument : "var" "yy"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "max" "room_height"
		var yy = (random_range(0, room_height));
	
		/// @DnDAction : YoYo Games.Common.Function_Call
		/// @DnDVersion : 1
		/// @DnDHash : 48C464E2
		/// @DnDInput : 6
		/// @DnDApplyTo : 1ea3458d-104e-4479-ad3d-b06f4b8c94c7
		/// @DnDParent : 6BE47D49
		/// @DnDArgument : "var" "boundCheck"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "function" "point_in_rectangle"
		/// @DnDArgument : "arg" "xx"
		/// @DnDArgument : "arg_1" "yy"
		/// @DnDArgument : "arg_2" "cameraX - pad"
		/// @DnDArgument : "arg_3" "cameraY - pad"
		/// @DnDArgument : "arg_4" "cameraX + cameraWidth + pad"
		/// @DnDArgument : "arg_5" "cameraY + cameraHeight + pad"
		with(obj_camera) {
			var boundCheck = point_in_rectangle(xx, yy, cameraX - pad, cameraY - pad, cameraX + cameraWidth + pad, cameraY + cameraHeight + pad);
		}
	}

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 4BAD0C3C
	/// @DnDParent : 07100AFE
	/// @DnDArgument : "xpos" "xx"
	/// @DnDArgument : "ypos" "yy"
	/// @DnDArgument : "objectid" "obj"
	instance_create_layer(xx, yy, "Instances", obj);
}