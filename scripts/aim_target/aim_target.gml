/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 2FC70778
/// @DnDInput : 4
/// @DnDArgument : "var" "range"
/// @DnDArgument : "value" "argument0"
/// @DnDArgument : "var_1" "bulletSpeed"
/// @DnDArgument : "value_1" "argument1"
/// @DnDArgument : "var_2" "moveToTarget"
/// @DnDArgument : "var_3" "keepDist"
var range = argument0;
var bulletSpeed = argument1;
var moveToTarget;
var keepDist;

/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 2DB396B9
/// @DnDArgument : "obj" "obj_ship"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "77f7923c-2459-4571-bc3f-515ee8648a72"
var l2DB396B9_0 = false;
l2DB396B9_0 = instance_exists(obj_ship);
if(!l2DB396B9_0)
{
	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 33A0EE99
	/// @DnDParent : 2DB396B9
	exit;
}

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 32121191
/// @DnDInput : 4
/// @DnDArgument : "var" "dist"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "function" "point_distance"
/// @DnDArgument : "arg" "x"
/// @DnDArgument : "arg_1" "y"
/// @DnDArgument : "arg_2" "obj_ship.x"
/// @DnDArgument : "arg_3" "obj_ship.y"
var dist = point_distance(x, y, obj_ship.x, obj_ship.y);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 305C02D7
/// @DnDArgument : "var" "dist"
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "range"
if(dist <= range)
{
	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 60207A29
	/// @DnDInput : 4
	/// @DnDParent : 305C02D7
	/// @DnDArgument : "var" "dir"
	/// @DnDArgument : "var_temp" "1"
	/// @DnDArgument : "function" "point_direction"
	/// @DnDArgument : "arg" "x"
	/// @DnDArgument : "arg_1" "y"
	/// @DnDArgument : "arg_2" "obj_ship.x"
	/// @DnDArgument : "arg_3" "obj_ship.y"
	var dir = point_direction(x, y, obj_ship.x, obj_ship.y);

	/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
	/// @DnDVersion : 1
	/// @DnDHash : 03DF35E1
	/// @DnDParent : 305C02D7
	/// @DnDArgument : "angle" "dir"
	image_angle = dir;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2B3D7A41
	/// @DnDParent : 305C02D7
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "bulletCounter"
	bulletCounter += 1;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7C255E58
	/// @DnDParent : 305C02D7
	/// @DnDArgument : "var" "bulletCounter"
	/// @DnDArgument : "op" "4"
	/// @DnDArgument : "value" "60"
	if(bulletCounter >= 60)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 42955182
		/// @DnDParent : 7C255E58
		/// @DnDArgument : "var" "bulletCounter"
		bulletCounter = 0;
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 18C4EC84
		/// @DnDInput : 2
		/// @DnDParent : 7C255E58
		/// @DnDArgument : "script" "create_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "bulletSpeed"
		/// @DnDSaveInfo : "script" "a9759563-8df4-4eba-a189-1d8d1b4f236a"
		script_execute(create_bullet, dir, bulletSpeed);
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 50FA46F1
else
{
	/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
	/// @DnDVersion : 1
	/// @DnDHash : 5484F1F0
	/// @DnDParent : 50FA46F1
	/// @DnDArgument : "angle" "direction"
	image_angle = direction;
}