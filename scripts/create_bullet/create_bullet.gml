/// @DnDAction : YoYo Games.Common.Temp_Variable
/// @DnDVersion : 1
/// @DnDHash : 6B1C0E90
/// @DnDInput : 3
/// @DnDArgument : "var" "dir"
/// @DnDArgument : "value" "argument0"
/// @DnDArgument : "var_1" "spd"
/// @DnDArgument : "value_1" "argument1"
/// @DnDArgument : "var_2" "gun_type"
/// @DnDArgument : "value_2" "-1"
var dir = argument0;
var spd = argument1;
var gun_type = -1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 31C4E9AC
/// @DnDArgument : "var" "argument_count"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "2"
if(argument_count > 2)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 675A7F6C
	/// @DnDParent : 31C4E9AC
	/// @DnDArgument : "expr" "argument2"
	/// @DnDArgument : "var" "gun_type"
	gun_type = argument2;
}

/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 7B93DFD1
/// @DnDArgument : "expr" "gun_type"
var l7B93DFD1_0 = gun_type;
switch(l7B93DFD1_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 58848D91
	/// @DnDParent : 7B93DFD1
	case 0:
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 12818352
		/// @DnDParent : 58848D91
		/// @DnDArgument : "soundid" "snd_zap"
		/// @DnDSaveInfo : "soundid" "0976ffcf-0650-4ed1-b4c7-54624008a26c"
		audio_play_sound(snd_zap, 0, 0);
	
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 679AA826
		/// @DnDParent : 58848D91
		/// @DnDArgument : "var" "sep"
		/// @DnDArgument : "value" "12"
		var sep = 12;
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 1DEA8F8D
		/// @DnDParent : 58848D91
		/// @DnDArgument : "xpos" "lengthdir_x(sep, dir - 90)"
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "lengthdir_y(sep, dir - 90)"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + lengthdir_x(sep, dir - 90), y + lengthdir_y(sep, dir - 90), "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 4737C09E
		/// @DnDInput : 3
		/// @DnDParent : 58848D91
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 6EFBC02F
		/// @DnDParent : 58848D91
		/// @DnDArgument : "xpos" "lengthdir_x(sep, dir + 90)"
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "lengthdir_y(sep, dir + 90)"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + lengthdir_x(sep, dir + 90), y + lengthdir_y(sep, dir + 90), "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 101DC3A2
		/// @DnDInput : 3
		/// @DnDParent : 58848D91
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 0D3679A2
	/// @DnDParent : 7B93DFD1
	/// @DnDArgument : "const" "1"
	case 1:
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 10F2890E
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "soundid" "snd_zap"
		/// @DnDSaveInfo : "soundid" "0976ffcf-0650-4ed1-b4c7-54624008a26c"
		audio_play_sound(snd_zap, 0, 0);
	
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 212CFF6E
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "var" "sep"
		/// @DnDArgument : "value" "12"
		var sep = 12;
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 21F8D884
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "xpos" "lengthdir_x(sep, dir - 90)"
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "lengthdir_y(sep, dir - 90)"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + lengthdir_x(sep, dir - 90), y + lengthdir_y(sep, dir - 90), "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 1FB97D22
		/// @DnDInput : 3
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 7F66F0BE
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "xpos" "lengthdir_x(sep, dir + 90)"
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "lengthdir_y(sep, dir + 90)"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + lengthdir_x(sep, dir + 90), y + lengthdir_y(sep, dir + 90), "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 304FD204
		/// @DnDInput : 3
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 4A44D773
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "xpos" "lengthdir_x(sep, dir)"
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "lengthdir_y(sep, dir)"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + lengthdir_x(sep, dir), y + lengthdir_y(sep, dir), "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 0863E15D
		/// @DnDInput : 3
		/// @DnDParent : 0D3679A2
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 76F6D2CB
	/// @DnDParent : 7B93DFD1
	/// @DnDArgument : "const" "2"
	case 2:
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 03947765
		/// @DnDParent : 76F6D2CB
		/// @DnDArgument : "soundid" "snd_zap"
		/// @DnDSaveInfo : "soundid" "0976ffcf-0650-4ed1-b4c7-54624008a26c"
		audio_play_sound(snd_zap, 0, 0);
	
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2002A120
		/// @DnDParent : 76F6D2CB
		/// @DnDArgument : "var" "sep"
		/// @DnDArgument : "value" "7"
		var sep = 7;
	
		/// @DnDAction : YoYo Games.Loops.For_Loop
		/// @DnDVersion : 1
		/// @DnDHash : 43BD87B8
		/// @DnDParent : 76F6D2CB
		/// @DnDArgument : "init_temp" "1"
		/// @DnDArgument : "cond" "i < 4"
		for(var i = 0; i < 4; i += 1) {
			/// @DnDAction : YoYo Games.Common.Temp_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 6E5C78C0
			/// @DnDParent : 43BD87B8
			/// @DnDArgument : "var" "bullet_angle"
			/// @DnDArgument : "value" "dir + (i * 45) - 65"
			var bullet_angle = dir + (i * 45) - 65;
		
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 1B1AEA2C
			/// @DnDParent : 43BD87B8
			/// @DnDArgument : "xpos" "lengthdir_x(sep, bullet_angle)"
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos" "lengthdir_y(sep, bullet_angle)"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "var" "newBullet"
			/// @DnDArgument : "var_temp" "1"
			/// @DnDArgument : "objectid" "obj_bullet"
			/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
			var newBullet = instance_create_layer(x + lengthdir_x(sep, bullet_angle), y + lengthdir_y(sep, bullet_angle), "Instances", obj_bullet);
		
			/// @DnDAction : YoYo Games.Common.Execute_Script
			/// @DnDVersion : 1.1
			/// @DnDHash : 3F455168
			/// @DnDInput : 3
			/// @DnDParent : 43BD87B8
			/// @DnDArgument : "script" "initialise_bullet"
			/// @DnDArgument : "arg" "bullet_angle"
			/// @DnDArgument : "arg_1" "spd"
			/// @DnDArgument : "arg_2" "newBullet"
			/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
			script_execute(initialise_bullet, bullet_angle, spd, newBullet);
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 7AB16D7D
	/// @DnDParent : 7B93DFD1
	/// @DnDArgument : "const" "3"
	case 3:
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 73847049
		/// @DnDParent : 7AB16D7D
		/// @DnDArgument : "soundid" "snd_zap"
		/// @DnDSaveInfo : "soundid" "0976ffcf-0650-4ed1-b4c7-54624008a26c"
		audio_play_sound(snd_zap, 0, 0);
	
		/// @DnDAction : YoYo Games.Common.Temp_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 085071EB
		/// @DnDParent : 7AB16D7D
		/// @DnDArgument : "var" "sep"
		/// @DnDArgument : "value" "7"
		var sep = 7;
	
		/// @DnDAction : YoYo Games.Loops.For_Loop
		/// @DnDVersion : 1
		/// @DnDHash : 3FBEC570
		/// @DnDParent : 7AB16D7D
		/// @DnDArgument : "init_temp" "1"
		/// @DnDArgument : "cond" "i < 8"
		for(var i = 0; i < 8; i += 1) {
			/// @DnDAction : YoYo Games.Common.Temp_Variable
			/// @DnDVersion : 1
			/// @DnDHash : 75B5D3FF
			/// @DnDParent : 3FBEC570
			/// @DnDArgument : "var" "bullet_angle"
			/// @DnDArgument : "value" "dir + (i * 45)"
			var bullet_angle = dir + (i * 45);
		
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 4566B56A
			/// @DnDParent : 3FBEC570
			/// @DnDArgument : "xpos" "lengthdir_x(sep, bullet_angle)"
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos" "lengthdir_y(sep, bullet_angle)"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "var" "newBullet"
			/// @DnDArgument : "var_temp" "1"
			/// @DnDArgument : "objectid" "obj_bullet"
			/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
			var newBullet = instance_create_layer(x + lengthdir_x(sep, bullet_angle), y + lengthdir_y(sep, bullet_angle), "Instances", obj_bullet);
		
			/// @DnDAction : YoYo Games.Common.Execute_Script
			/// @DnDVersion : 1.1
			/// @DnDHash : 26FF294B
			/// @DnDInput : 3
			/// @DnDParent : 3FBEC570
			/// @DnDArgument : "script" "initialise_bullet"
			/// @DnDArgument : "arg" "bullet_angle"
			/// @DnDArgument : "arg_1" "spd"
			/// @DnDArgument : "arg_2" "newBullet"
			/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
			script_execute(initialise_bullet, bullet_angle, spd, newBullet);
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 1B389A1B
	/// @DnDParent : 7B93DFD1
	/// @DnDArgument : "const" "4"
	case 4:
		/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
		/// @DnDVersion : 1
		/// @DnDHash : 58BAB9DE
		/// @DnDParent : 1B389A1B
		/// @DnDArgument : "soundid" "snd_laser"
		/// @DnDArgument : "not" "1"
		/// @DnDSaveInfo : "soundid" "7c985564-2284-47f1-92d3-b39d7c2958a1"
		var l58BAB9DE_0 = snd_laser;
		if (!audio_is_playing(l58BAB9DE_0))
		{
			/// @DnDAction : YoYo Games.Audio.Play_Audio
			/// @DnDVersion : 1
			/// @DnDHash : 7FF854A7
			/// @DnDParent : 58BAB9DE
			/// @DnDArgument : "soundid" "snd_laser"
			/// @DnDSaveInfo : "soundid" "7c985564-2284-47f1-92d3-b39d7c2958a1"
			audio_play_sound(snd_laser, 0, 0);
		}
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 2EE92E21
		/// @DnDParent : 1B389A1B
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_laser"
		/// @DnDSaveInfo : "objectid" "bab36f59-3944-4dfe-97fc-60dc96c752ba"
		var newBullet = instance_create_layer(x + 0, y + 0, "Instances", obj_laser);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 7B2F370A
		/// @DnDInput : 3
		/// @DnDParent : 1B389A1B
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "0"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, 0, newBullet);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 6D98BEFE
	/// @DnDParent : 7B93DFD1
	/// @DnDArgument : "const" "6"
	case 6:
		/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
		/// @DnDVersion : 1
		/// @DnDHash : 30221505
		/// @DnDParent : 6D98BEFE
		/// @DnDArgument : "soundid" "snd_laser"
		/// @DnDArgument : "not" "1"
		/// @DnDSaveInfo : "soundid" "7c985564-2284-47f1-92d3-b39d7c2958a1"
		var l30221505_0 = snd_laser;
		if (!audio_is_playing(l30221505_0))
		{
			/// @DnDAction : YoYo Games.Audio.Play_Audio
			/// @DnDVersion : 1
			/// @DnDHash : 64B4B9FA
			/// @DnDParent : 30221505
			/// @DnDArgument : "soundid" "snd_laser"
			/// @DnDSaveInfo : "soundid" "7c985564-2284-47f1-92d3-b39d7c2958a1"
			audio_play_sound(snd_laser, 0, 0);
		}
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 32847A7B
		/// @DnDParent : 6D98BEFE
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_laser_huge"
		/// @DnDSaveInfo : "objectid" "75d7e375-26c7-40d1-a3b4-c0ae707c1833"
		var newBullet = instance_create_layer(x + 0, y + 0, "Instances", obj_laser_huge);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 66FFD83E
		/// @DnDInput : 3
		/// @DnDParent : 6D98BEFE
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "0"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, 0, newBullet);
		break;

	/// @DnDAction : YoYo Games.Switch.Default
	/// @DnDVersion : 1
	/// @DnDHash : 42FF9870
	/// @DnDParent : 7B93DFD1
	default:
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 47AC3A0A
		/// @DnDParent : 42FF9870
		/// @DnDArgument : "soundid" "snd_zap"
		/// @DnDSaveInfo : "soundid" "0976ffcf-0650-4ed1-b4c7-54624008a26c"
		audio_play_sound(snd_zap, 0, 0);
	
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 72D06A91
		/// @DnDParent : 42FF9870
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "var" "newBullet"
		/// @DnDArgument : "var_temp" "1"
		/// @DnDArgument : "objectid" "obj_bullet"
		/// @DnDSaveInfo : "objectid" "b2f3e12f-832a-465f-a232-32feeea761a1"
		var newBullet = instance_create_layer(x + 0, y + 0, "Instances", obj_bullet);
	
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 2DA2D33B
		/// @DnDInput : 3
		/// @DnDParent : 42FF9870
		/// @DnDArgument : "script" "initialise_bullet"
		/// @DnDArgument : "arg" "dir"
		/// @DnDArgument : "arg_1" "spd"
		/// @DnDArgument : "arg_2" "newBullet"
		/// @DnDSaveInfo : "script" "26081418-f4c2-4258-b06c-cfe723146e30"
		script_execute(initialise_bullet, dir, spd, newBullet);
		break;
}