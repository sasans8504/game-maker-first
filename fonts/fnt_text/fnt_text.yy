{
    "id": "a7193389-479d-4eaf-b8fc-ceb5989fb3a5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ce433c68-4234-4e24-bcc8-5e4af7ce55f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e0926e2e-ae22-4d82-9ed5-f624d90569c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 207,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f232311f-a9a6-4a65-90bf-84d4a01eb4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 199,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a7a2a1a8-ca31-439d-9a4a-e95870cc0eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 187,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b7cc7e84-9ad7-4f53-8561-574ddf181ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 176,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "02da3470-cb93-4949-a9e6-805e87811ac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 164,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d62f596b-33e7-4704-87eb-78a3ad457eb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cf066552-ad17-4456-b9fb-51ec6de9bf94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 146,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "90587aaf-9910-4fc0-bd89-2aaed013efa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 139,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0cc740f3-eac3-436b-9034-ecb4f2cbce53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 131,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a2f7619f-e6da-4347-b071-c2c80d118439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1af1d930-1823-4ee2-ab6a-f27945a35cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fba8fd8f-4ac6-4e49-af19-0639cba02078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7e4ed545-14bd-498f-8b66-fd287184206f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 95,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "18899edb-7143-42f8-806e-077faf3bbbed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 89,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ebd5bb19-171a-42ff-9aba-4e757dd3e46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b49b67a2-dbb4-4862-bcd6-f737562c685e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3875eadf-4cf5-4bdd-b553-1e217e836ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "94304110-f1af-41cf-9a6e-54aeb43d6ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d96880f8-8f17-406e-8616-2a27659b6a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9f4d53a2-27c2-4f89-b9d4-daf8f80112d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "199b9cc1-ee8f-4a4e-8f6f-0d444dd05cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ad9961db-3ecd-46de-ac41-9c2bb4ce9dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "09c0d13c-c261-476e-9452-c3de6aca0e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 233,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "95f296ac-25d8-4197-80ae-0a01c1bc4d01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a49e8f73-329f-4165-9412-f4d3786f62c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 199,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ee1b64c3-eada-48db-8613-f17821261235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 194,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6fd23638-00ba-4be5-aefd-d9e974064f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 187,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9df2480a-6de4-4924-bc07-8187b415b39d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9120af0c-71da-4119-925a-10df60125dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 167,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b4aacd09-d033-4a43-b4f0-6ba19c2b13ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 157,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a9ef48ec-6c94-4438-a229-a57b0031dc90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 149,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f809209a-3da2-490d-b1a1-60adad0e574e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cc24bf98-8694-434c-a741-4be115ff217f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d0efeb48-0461-4eaf-a779-f52260f9ec64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1be165f8-99a9-46bc-ad21-65622c12b1c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 104,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d01b0519-7795-49a8-b281-49f531f2f146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d83cd98b-3b08-49cd-ad3b-833137ada422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 84,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b81552a5-9cfc-4575-a3a5-85855dd4af37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c3be0b30-0457-4999-9bbb-b66dc0b94eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3a007965-53c3-47f9-be2b-cf7c73745861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5dfc1601-7cdd-4a24-b08d-0e944360b072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "46731808-4f4d-4ff5-8374-91373774bbec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "94144a77-6bce-4082-ac4f-227650954646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 23,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "903793e4-bd3a-4e1d-b6fe-d64133b37811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "08f3bff1-07f3-428c-908b-bf88583dc932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4d83e742-053d-4672-97d5-e80f4b54dc64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9d4ec31d-9a48-4691-9021-ec106f51d630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bd027c8a-3f62-4012-bfb8-1c89d4289bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 236,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4306dcc4-df46-4fc7-94da-6f11a1307979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "06a5c5e3-24aa-40f9-b899-47a53afbe23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0ed56f57-6d6d-4203-acfb-53d2a9c04c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e246e8ac-9555-405c-ad31-850c1e1600d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cc7e6682-f65e-4c87-8d5b-e4c838b14f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "73e0bd1e-122a-4b01-a685-d9238870313e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "098fe481-35cb-4a01-b34f-2ec9dfc5df0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "78966fcc-8e71-4f49-9166-8c68f8213255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "46822581-87f8-4f92-aedd-35a3c11b132b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "60576cec-01d5-4e72-9343-e19081cf67f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fb1ae925-8a22-46db-9b1a-7f861f6af9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "79f74071-12bf-4ba9-83ae-84f35e8e41a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5e1bf26b-135e-4ba5-a612-fce2062424df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9e48f4a9-bf21-44c0-94ec-361de6947582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "267f84b0-2dc7-4b16-8f9c-e2c44385e415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4800a0a3-8ea2-475f-a07b-0485105c8e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a4011b8b-e00b-4bb2-a4c6-de7150e1e2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "01637089-4ebb-46c5-84c8-62291f10b261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5aae2855-aaf4-4264-b28d-a38cc19785be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "94752b02-7a80-456c-b419-81c0f0ae0394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "dd9348ba-6424-4877-86ed-1517c0997f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fc488166-8269-46e4-937e-6dc3e2432847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3830bca6-fec6-4771-b013-fc6ddc5e2c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9fd1408e-dacc-47b1-86a1-0fd62f6d96ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fc163316-a103-481b-9ec3-22b1ec7a483e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "219b98c2-550c-4f94-aadc-64547e3a43aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 217,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "05850052-dcdc-479b-8c5b-ccfeb42e56ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 206,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e2f93dca-a81f-4590-8c73-a65f1db62ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "41c5c1ce-2341-4ddd-b21f-ad19eabcf0c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 185,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a9bf5b19-2880-43e4-b2b8-292705ed71d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 175,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7b1252a1-222b-4c98-a92a-1dd4f212c683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 164,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "aeb4e933-7603-4198-b274-5e07f55a072d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 153,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "708a55f1-a4d9-4fe2-8ec0-d8f9cc0ac3a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 143,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c02aa04b-1720-4730-97bf-6dd3a968d922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 133,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "31adfc1b-7953-4f3b-b157-3104ac6809fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e006970d-c2e5-4638-bbee-5f5ffb63c9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 123,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d2b39a50-7aea-4adb-a994-c1f16c250263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 103,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f5e46f0c-fbe7-4e18-8238-36215d7c50c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 91,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8cbc120e-26d7-4cc4-9687-fe50b82ff805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 79,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "de8df2ca-e866-45b4-afc4-27b7728ea452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "152c26a1-f066-49ba-9084-8427c3129e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "797bf8da-2a83-4a87-9339-4318b2fdf6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d42068c5-168d-4cbb-bc77-63108555d0a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d6b413d6-20dc-420c-a75f-ff3af9df8b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 32,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bd367356-1677-4de7-85dd-3a12dfe2809c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 23,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "58f1b51e-ae51-4068-b269-ff328b5d333f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 209,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c0ba6bb0-7858-4c75-afab-b39151674708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 221,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}